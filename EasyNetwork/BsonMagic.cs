﻿namespace EasyNetwork
{
    using System.IO;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Bson;

    /// <summary>
    /// Class which serializes and deserializes objects to/from Binary JSON
    /// </summary>
    public static class BsonMagic
    {
        /// <summary>
        /// Serialize an object into Binary JSON
        /// </summary>
        /// <typeparam name="T">Type of the object being serialized</typeparam>
        /// <param name="objectToSerialize">The object to be serialized</param>
        /// <returns>An array of bytes containing the serialized object</returns>
        public static byte[] SerializeObject<T>(T objectToSerialize)
        {
            byte[] data;

            using (MemoryStream ms = new MemoryStream())
            {
                using (BsonWriter writer = new BsonWriter(ms))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(writer, objectToSerialize);
                    data = ms.ToArray();
                }
            }

            return data;
        }

        /// <summary>
        /// Deserializes an object from Binary JSON
        /// </summary>
        /// <typeparam name="T">Type of the object to deserialize</typeparam>
        /// <param name="binaryData">Byte array containing the previously serialized object</param>
        /// <returns>The deserialized object</returns>
        public static T DeserializeObject<T>(byte[] binaryData)
        {
            T deserialziedObject;

            using (MemoryStream ms = new MemoryStream(binaryData))
            {
                using (BsonReader reader = new BsonReader(ms))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    deserialziedObject = serializer.Deserialize<T>(reader);
                }
            }

            return deserialziedObject;
        }
    }
}
