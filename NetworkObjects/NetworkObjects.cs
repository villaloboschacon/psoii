﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace NetworkObjects
{
    public class Game
    {
        public class CreateMatch
        {
            public CreateMatch()
            {
                IDMatch = new Guid();
                pass = "";
                nombre = "";
                username = "";
            }
            public Guid IDMatch { get; set; }
            public String pass { get; set; }
            public String nombre { get; set; }
            public String username { get; set; }
        }
        public class JoinMatch
        {
            public JoinMatch()
            {
                password = "";
                username = "";
            }
            public String password { get; set; }
            public String username { get; set; }
        }
        public class Match
        {
            public Match()
            {
                IDMatch = new Guid();
                password = "";
                players = new List<PlayerListing.Player>();
                estado = false;
            }
            public Guid IDMatch { get; set; }
            public String password { get; set; }
            public List<PlayerListing.Player> players { get; set; }
            public bool estado { get; set; }
        }
        //Respuesta si se crea o no
        public class GameResponse
        {
            public GameResponse()
            {
                tipo = "";
                estado = false;
                idpartida = new Guid();
            }
            public String tipo { get; set; }
            public bool estado { get; set; }
            public Guid idpartida { get; set; }
        }
        public Game()
        {
            matches = new List<Match>();
        }
        public bool verificaMatch(String password)
        {
            foreach (NetworkObjects.Game.Match match in matches)
            {
                if (password == match.password)
                {
                    return true;
                }
            }
            return false;
        }
        public bool verificaCrearPartida(String password)
        {
            foreach (NetworkObjects.Game.Match match in matches)
            {
                if (password == match.password)
                {
                    return false;
                }
            }
            return true;
        }
        public List<Match> matches { get; set; }
    }
    public class ListPlayersMatch
    {
        public ListPlayersMatch(Game.Match match)
        {
            if (match != null)
            {
                players = match.players;
            }
            else
            {
                players = new List<PlayerListing.Player>();
            }
        }
        public List<PlayerListing.Player> players { get; set; }
    }
    public class OpenTable
    {
        public OpenTable()
        {
            idpartida = new Guid();
            response = false;
            name = "";
            username = "";
        }
        public string username { get; set; }
        public string name { get; set; }
        public Guid idpartida { get; set; }
        public bool response { get; set; }
    }
    public class UserRequest
    {
        public UserRequest()
        {
            player = new PlayerListing.Player();
        }
        public PlayerListing.Player player { get; set; }
    }
    public class Online
    {
        public Online()
        {
            username = "";
            name = "";
            online = false;
            inmatch = false;
            countdown = false;
        }
        public string username { get; set; }
        public string name { get; set; }
        public bool online { get; set; }
        public bool inmatch { get; set; }
        public bool countdown { get; set; }
    }
    public class Login
    {
        public Login()
        {
            name = "";
            username = "";
        }
        public String name { get; set; }
        public String username { get; set; }
    }
    public class LoginResponse
    {
        public LoginResponse()
        {
            Success = false;
        }
        public bool Success { get; set; }
    }
    public class JoinGame
    {
        public string Name { get; set; }
        public string username { get; set; }
        public int bet { get; set; }
    }
    public class DealerResponse
    {
        public bool estado { get; set; }

        public DealerResponse()
        {
            estado = false;
        }
    }
    public class DealerRequest
    {
        public bool response { get; set; }
        public int numero { get; set; }

        public DealerRequest()
        {
            response = false;
            numero = 0;
        }
    }
    public class UpdateBet
    {
        public UpdateBet()
        {
            bet = 0;
            name = "";
            username = "";
            cliendId = new Guid();
        }
        public int bet { get; set; }
        public string name { get; set; }
        public string username { get; set; }
        public Guid cliendId { get; set; }
    }
    public class BetResponse
    {
        public string estado { get; set; }
    }
    public class Username
    {
        public string user { get; set; }
    }
    public class UsernameResponse
    {
        public bool Success { get; set; }
    }

    public class JoinGameResponse
    {
        public bool Success { get; set; }
        public string ResponseMessage { get; set; }
    }

    public class PlayerJoined
    {
        public string Name { get; set; }
        public Guid clientId { get; set; }
        public String username { get; set; }
        public int bet { get; set; }
    }
    public class Request
    {
        public string type { get; set; }
        public bool Success { get; set; }

        public class Player
        {
            public String Name { get; set; }
            public String username { get; set; }
            public Guid clientId { get; set; }
            public int bet { get; set; }
        }

        public List<Player> GetPlayers()
        {
            return Players;
        }
        public List<Player> Players { get; set; }
    }
    public class Turn
    {
        public Turn()
        {
            isDone = false;
            response = "";
            myturn = false;
            name = "";
            username = "";
        }
        public string username { get; set; }
        public string name { get; set; }
        public string response { get; set; }
        public bool isDone { get; set; }
        public bool myturn { get; set; }
    }
        public class PlayerListing
        {
            public class Player
            {
                public String Name { get; set; }
                public Guid clientId { get; set; }
                public String username { get; set; }
                public int bet { get; set; }
                public List<Card> hand;
                public bool online { get; set; }
                public bool inmatch { get; set; }
                public bool countdown { get; set; }
                public bool turn { get; set; }
                public Player()
                {
                    Name = "";
                    clientId = new Guid();
                    username = "";
                    bet = 0;
                    hand = new List<Card>();
                    online = false;
                    inmatch = false;
                    countdown = false;
                    turn = false;
                }
            }
            public class Dealer
            {
                public List<Card> hand;
                public Dealer()
                {
                    hand = new List<Card>();
                }
            }
            public class Card
            {
                public int number { get; set; }
                public char type { get; set; }
                public string suit { get; set; }

                public Card()
                {
                    number = 0;
                    type = ' ';
                    suit = "";
                }
            }
            public class UpdateListPlayers
            {
                public UpdateListPlayers()
                {
                    Players = new List<Player>();
                }
                public List<Player> Players { get; set; }
            }
            public PlayerListing()
            {
                Players = new List<Player>();
                Dealers = new Dealer();
                dealerResponse = new DealerResponse();
                dealerReq = new DealerRequest();
                contadorRequest = 0;
            }
            public bool dealerRequest()
            {
                if (Dealers.hand == null)
                {
                    dealerReq.numero++;
                    return true;
                }
                return false;
            }
            public int NumPlayers()
            {
                return Players.Count;
            }
            public List<Player> GetPlayers()
            {
                return Players;
            }
            public void AddPlayer(Player p)
            {
                Players.Add(p);
            }
            public void AddPlayer(String name, String username, Guid _clientId)
            {
                Player p = new Player();
                p.Name = name;
                p.username = username;
                p.clientId = _clientId;
                Players.Add(p);
            }
            public void DeletePlayer(Guid _clientId)
            {
                Player p = new Player();
                p.clientId = _clientId;
                for (int i = 0; i < Players.Count(); i++)
                {
                    if (Players[i].clientId == p.clientId)
                    {
                        Players.RemoveAt(i);
                    }
                }
            }
            public int contadorRequest { get; set; }
            public DealerResponse dealerResponse { get; set; }
            public DealerRequest dealerReq { get; set; }
            public List<Player> Players { get; set; }
            public Dealer Dealers { get; set; }
        }
    }
