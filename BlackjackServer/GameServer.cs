﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackjackServer
{
    class GameServer
    {
        private EasyNetwork.Server server = new EasyNetwork.Server("tcp://127.0.0.1:3000");
        private bool isRunning = true;

        private const int MAX_PLAYERS = 5;
        private NetworkObjects.PlayerListing playerListing;
        private int contadorRequest;
        private int turno;
        private NetworkObjects.Game game;

        public GameServer()
        {
            turno = 0;
            NetworkObjects.PlayerListing.Player player = new NetworkObjects.PlayerListing.Player();
            player.Name = "Steven";
            player.username = "1";
            this.playerListing = new NetworkObjects.PlayerListing();
            playerListing.AddPlayer(player);
            this.game = new NetworkObjects.Game();
        }

        public void start()
        {
            server.DataReceived += Server_DataReceived;
            server.Start();

            Console.WriteLine("Blackjack game server now running...");

            while (isRunning)
            {
                System.Threading.Thread.Sleep(1000);
            }

            server.Stop();
        }

        public void Stop()
        {
            isRunning = false;
        }

        private void Server_DataReceived(object receivedObject, Guid clientId)
        {
            // Recieve join game message
            if (receivedObject is NetworkObjects.Turn)
            {
                NetworkObjects.Turn turn = receivedObject as NetworkObjects.Turn;
                int posicion = verificaMatch(turn.username, turn.name);
                if (posicion != -1)
                {
                    if (turno == (game.matches[posicion].players.Count())+1)
                    {
                        turn.myturn = false;
                        turn.isDone = false;
                        turn.response = "dealer";
                        turno = 0;
                    }
                    else
                    {
                        if (game.matches[posicion].players[turno].Name == turn.name)
                        {
                            turn.myturn = true;
                            turn.isDone = false;
                            turn.response = "comienza";
                        }
                        else
                        {
                            turn.myturn = false;
                            turn.isDone = false;
                        }
                        if (turno <= game.matches.Count())
                        {
                            turno++;
                        }
                    }
                }
                else
                {
                    turn.myturn = false;
                    turn.isDone = false;
                }
                server.Send(turn,clientId);
            }
            if (receivedObject is NetworkObjects.OpenTable)
            {
                bool found = false;
                NetworkObjects.OpenTable openTable = receivedObject as NetworkObjects.OpenTable;
                foreach (NetworkObjects.Game.Match match in this.game.matches)
                {
                    if (match.IDMatch == openTable.idpartida)
                    {
                        openTable.response = true;
                        NetworkObjects.Game.Match partida = new NetworkObjects.Game.Match();
                        partida = game.matches[verificaMatch(openTable.username, openTable.name)];
                        NetworkObjects.ListPlayersMatch listPlayersMatch = 
                            new NetworkObjects.ListPlayersMatch(partida);
                        server.Send(listPlayersMatch, clientId);
                        server.Send(openTable, clientId);
                        found = true;
                    }
                }
                if (!found)
                {
                    openTable.response = false;
                    //server.Send(match, clientId);
                    server.Send(openTable.response, clientId);
                }
            }
            else if (receivedObject is NetworkObjects.Online)
            {
                NetworkObjects.Online online = receivedObject as NetworkObjects.Online;
                bool flag = false;
                int posicion = verificaMatch(online.username, online.name);
                if (posicion != -1)
                {
                    if (verificaInmatch(online))
                    {
                        Console.WriteLine("El usuario " + online.username + " esta listo para la partida. \n-------------------------------------------------------");
                    }
                    else
                    {
                        Console.WriteLine("El usuario " + online.username + " no se encuentra disponible. \n-------------------------------------------------------");
                    }
                    NetworkObjects.Game.Match match = new NetworkObjects.Game.Match();
                    match = game.matches[posicion];
                    foreach (NetworkObjects.PlayerListing.Player player in this.game.matches[posicion].players)
                    {
                        if (player.inmatch)
                        {
                            flag = true;
                        }
                        else
                        {
                            flag = false;
                        }
                    }
                    if (verificaCountDown(posicion))
                    {
                        NetworkObjects.Game.GameResponse gameResponse = new NetworkObjects.Game.GameResponse();
                        gameResponse.estado = true;
                        gameResponse.tipo = "COUNTDOWN";
                        foreach (NetworkObjects.PlayerListing.Player player in this.game.matches[posicion].players)
                        {
                            server.Send(gameResponse, clientId);
                        }
                    }

                }
            }
            else if (receivedObject is NetworkObjects.UserRequest)
            {
                NetworkObjects.UserRequest userRequest = receivedObject as NetworkObjects.UserRequest;

                foreach (NetworkObjects.PlayerListing.Player player in this.playerListing.Players)
                {
                    if (player.username == userRequest.player.username)
                    {
                        userRequest.player = player;
                    }
                }
                server.Send(userRequest.player, clientId);
            }
            else if (receivedObject is NetworkObjects.Login)
            {
                NetworkObjects.Login login = receivedObject as NetworkObjects.Login;
                if (verificaLogin(login.username, login.name))
                {
                    NetworkObjects.LoginResponse loginResponse = new NetworkObjects.LoginResponse();
                    loginResponse.Success = true;
                    server.Send(loginResponse, clientId);
                }
                else
                {
                    NetworkObjects.LoginResponse loginResponse = new NetworkObjects.LoginResponse();
                    loginResponse.Success = false;
                    server.Send(loginResponse, clientId);
                }

            }
            else if (receivedObject is NetworkObjects.Game.JoinMatch)
            {
                Console.WriteLine("Solicitud para unirse a la partida de " + clientId.ToString() + "\n");
                NetworkObjects.Game.JoinMatch joinMatch = receivedObject as NetworkObjects.Game.JoinMatch;
                int i = 0;
                bool flag = true;
                foreach (NetworkObjects.Game.Match match in this.game.matches)
                {
                    if (match.password == joinMatch.password)
                    {
                        for (int k = 0; k < game.matches.Count(); k++)
                        {
                            for (int t = 0; t < game.matches[k].players.Count(); t++)
                            {
                                if (game.matches[k].players[t].username == joinMatch.username)
                                {
                                    flag = false;
                                }
                            }
                        }
                        if (flag)
                        {
                            NetworkObjects.PlayerListing.Player p = new NetworkObjects.PlayerListing.Player();
                            foreach (NetworkObjects.PlayerListing.Player player in this.playerListing.Players)
                            {
                                if (player.username == joinMatch.username)
                                {
                                    p = player;
                                }
                            }
                            Console.WriteLine(p.username + " se ha unido a la partida de " + game.matches[i].players[0].username + "\n----------------------------------------------");
                            this.game.matches[i].players.Add(p);
                            NetworkObjects.Game.GameResponse gameResponse = new NetworkObjects.Game.GameResponse();
                            gameResponse.idpartida = game.matches[i].IDMatch;
                            gameResponse.estado = true;
                            gameResponse.tipo = "UNIRSE";
                            server.Send(gameResponse, clientId);
                        }
                        else
                        {
                            Console.WriteLine(joinMatch.username + " ya esta en la partida de " + game.matches[i].players[0].username + "\n----------------------------------------------");
                            NetworkObjects.Game.GameResponse gameResponse = new NetworkObjects.Game.GameResponse();
                            gameResponse.idpartida = game.matches[i].IDMatch;
                            gameResponse.estado = true;
                            gameResponse.tipo = "UNIRSE";
                            server.Send(gameResponse, clientId);
                        }
                    }
                    i++;
                }
            }
            else if (receivedObject is NetworkObjects.Game.CreateMatch)
            {
                Console.WriteLine("Solicitud de partida de " + clientId.ToString() + "\n");
                NetworkObjects.Game.CreateMatch newGame = receivedObject as NetworkObjects.Game.CreateMatch;
                NetworkObjects.Game.Match match = new NetworkObjects.Game.Match();
                NetworkObjects.Game.GameResponse gameResponse = new NetworkObjects.Game.GameResponse();
                newGame.IDMatch = Guid.NewGuid();
                if (game.verificaCrearPartida(newGame.pass))
                {
                    Console.WriteLine("Password de partida correcta..\n-----------------------------------\n");
                    match.estado = false;
                    match.IDMatch = newGame.IDMatch;
                    match.password = newGame.pass;
                    NetworkObjects.PlayerListing.Player p = new NetworkObjects.PlayerListing.Player();
                    p.Name = newGame.nombre;
                    p.username = newGame.username;
                    p.clientId = clientId;
                    match.players.Add(p);
                    game.matches.Add(match);
                    Console.WriteLine("Dueno de partida: " + p.username + ". \n-----------------------------------\n");
                    Console.WriteLine("Partida disponible..\n-----------------------------------\n");
                    gameResponse.idpartida = newGame.IDMatch;
                    gameResponse.estado = true;
                    gameResponse.tipo = "CREAR";
                    server.Send(gameResponse, clientId);
                }
                else
                {
                    Console.WriteLine("Password de partida incorrecta...\n-----------------------------------\n");
                    gameResponse.estado = false;
                    server.Send(gameResponse, clientId);
                }

            }
            else if (receivedObject is NetworkObjects.PlayerListing)
            {
                NetworkObjects.PlayerListing listing = receivedObject as NetworkObjects.PlayerListing;
                playerListing.Dealers = listing.Dealers;

                for (int i = 0; i < listing.Players.Count(); i++)
                {
                    if (playerListing.Players[i].hand.Count() <= listing.Players[i].hand.Count())
                    {
                        playerListing.Players[i].hand = new List<NetworkObjects.PlayerListing.Card>();
                    }
                    for (int j = 0; j < listing.Players[i].hand.Count(); j++)
                    {
                            NetworkObjects.PlayerListing.Card card = new NetworkObjects.PlayerListing.Card();
                            card.suit = listing.Players[i].hand[j].suit;
                            card.number = listing.Players[i].hand[j].number;
                            card.type = listing.Players[i].hand[j].type;
                            playerListing.Players[i].hand.Add(card);
                    }
                }
                //game.matches[0].players = playerListing.Players;
            }
            else if (receivedObject is NetworkObjects.DealerRequest)
            {
                NetworkObjects.DealerRequest dealerRequest = receivedObject as NetworkObjects.DealerRequest;
                contadorRequest++;
                dealerRequest.numero = contadorRequest;
                NetworkObjects.DealerResponse dealerResponse = new NetworkObjects.DealerResponse();
                if (dealerRequest.numero != 1)
                {
                    dealerResponse.estado = false;
                }
                else
                {
                    dealerResponse.estado = true;
                }
                server.Send(dealerResponse, clientId);
            }
            else if (receivedObject is NetworkObjects.UpdateBet)
            {
                bool flag = true;
                NetworkObjects.UpdateBet updateBet = receivedObject as NetworkObjects.UpdateBet;
                updateBet.cliendId = clientId;
                foreach (NetworkObjects.PlayerListing.Player player in this.game.matches[verificaMatch(updateBet.username, updateBet.name)].players)
                {
                    if (updateBet.username == player.username)
                    {
                        player.bet = updateBet.bet;
                    }
                    if (player.bet == 0)
                    {
                        flag = false;
                    }
                }
                NetworkObjects.BetResponse betResponse = new NetworkObjects.BetResponse();
                if (flag)
                {
                    betResponse.estado = "listo";
                }
                else
                {
                    betResponse.estado = "espera";
                }
                server.Send(betResponse, clientId);
                //                sendAllList(verificaMatch(updateBet.username, updateBet.name));
                foreach (NetworkObjects.Game.Match match in this.game.matches)
                {
                    foreach (NetworkObjects.PlayerListing.Player player in this.game.matches[verificaMatch(updateBet.username, updateBet.name)].players)
                    {
                        NetworkObjects.Game.Match matchss = game.matches[verificaMatch(updateBet.username, updateBet.name)];
                        NetworkObjects.ListPlayersMatch listPlayersMatch = new NetworkObjects.ListPlayersMatch(matchss);
                        listPlayersMatch.players = match.players;
                        server.Send(listPlayersMatch, player.clientId);
                    }
                }

                //server.Send(game.matches[verificaMatch(updateBet.username, updateBet.name)], clientId);
            }
            else if (receivedObject is NetworkObjects.Username)
            {
                NetworkObjects.Username username = receivedObject as NetworkObjects.Username;
                int cont = 0;
                Console.WriteLine("Solicitud de usuario de " + clientId.ToString());
                foreach (NetworkObjects.PlayerListing.Player player in this.playerListing.Players)
                {
                    if (player.username == username.user)
                    {
                        cont++;
                    }
                }
                NetworkObjects.UsernameResponse usernameresponse = new NetworkObjects.UsernameResponse();
                if (cont > 0)
                {
                    Console.WriteLine("El usuario " + username.user + " se encuentra ocupado.\n--------------------------------------\n");
                    usernameresponse.Success = false;
                    server.Send(usernameresponse, clientId);
                }
                else
                {
                    Console.WriteLine("El usuario " + username.user + " se encuentra disponible.\n--------------------------------------\n");
                    usernameresponse.Success = true;
                    server.Send(usernameresponse, clientId);
                }
            }
            else if (receivedObject is NetworkObjects.JoinGame)
            {
                Console.WriteLine("Incomming request from " + clientId.ToString());

                NetworkObjects.JoinGame joinMsg = receivedObject as NetworkObjects.JoinGame;

                if (this.playerListing.NumPlayers() >= MAX_PLAYERS)
                {
                    SendPlayerJoinFailedMessage(clientId, "Game is currently full! Please try again later!");
                    return;
                }
                if (verificaUsername(joinMsg.username))
                {
                    AddPlayerToGame(joinMsg.Name, joinMsg.username, clientId);
                    NetworkObjects.JoinGameResponse mJoinResponse = new NetworkObjects.JoinGameResponse();
                    mJoinResponse.ResponseMessage = "EXITO!";
                    mJoinResponse.Success = true;
                    server.Send(mJoinResponse, clientId);
                }
                else
                {
                    NetworkObjects.JoinGameResponse mJoinResponse = new NetworkObjects.JoinGameResponse();
                    mJoinResponse.Success = false;
                    mJoinResponse.ResponseMessage = "FALLIDO!";
                    server.Send(mJoinResponse, clientId);
                    Console.WriteLine("Usuario ya existente.");
                }
            }
            else if (receivedObject is NetworkObjects.Request)
            {
                NetworkObjects.Request request = receivedObject as NetworkObjects.Request;
                if (request.type == "lista")
                {
                    SendPlayerListingToPlayer(clientId);
                }
                else if (request.type == "salir")
                {
                    DeletePlayer(clientId);
                }
                else if (request.type == "nuevo")
                {
                    newGame(clientId);
                }
            }
        }
        private void sendAll()
        {
            foreach (NetworkObjects.Game.Match match in this.game.matches)
            {
                for (int i = 0; i < game.matches[i].players.Count(); i++)
                {
                    server.Send(match, match.players[i].clientId);
                }
            }
        }
        private void sendAllList(int posicion)
        {
            foreach (NetworkObjects.Game.Match match in this.game.matches)
            {
                foreach (NetworkObjects.PlayerListing.Player player in this.game.matches[posicion].players)
                {
                    server.Send(match.players, player.clientId);
                }
            }
        }
        private int verificaMatch(String username, String name)
        {
            for (int i = 0; i < game.matches.Count(); i++)
            {
                foreach (NetworkObjects.PlayerListing.Player player in this.game.matches[i].players)
                {
                    if (player.username == username && player.Name == name)
                    {
                        return i;
                    }
                }
            }
            return -1;
        }
        private bool verificaInmatch(NetworkObjects.Online online)
        {
            for (int i = 0; i < game.matches.Count(); i++)
            {
                foreach (NetworkObjects.PlayerListing.Player player in this.game.matches[i].players)
                {
                    if (player.username == online.username)
                    {
                        player.inmatch = online.inmatch;
                        player.online = online.online;
                        player.countdown = online.countdown;
                        if (player.inmatch)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        private bool verificaCountDown(int posicion)
        {
            bool x = true;
            foreach (NetworkObjects.PlayerListing.Player player in this.game.matches[posicion].players)
            {
                if (!player.countdown)
                {
                    x = false;
                }
            }
            return x;
        }
        private bool verificaUsername(String username)
        {
            int cont = 0;
            foreach (NetworkObjects.PlayerListing.Player player in this.playerListing.Players)
            {
                if (player.username == username)
                {
                    cont++;
                }
            }
            if (cont == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool verificaLogin(String username, String name)
        {
            foreach (NetworkObjects.PlayerListing.Player player in this.playerListing.Players)
            {
                if (player.Name == name && player.username == username)
                {
                    return true;
                }
            }
            return false;
        }
        private void newGame(Guid clientId)
        {
            foreach (NetworkObjects.PlayerListing.Player player in this.playerListing.Players)
            {
                player.bet = 0;
                player.hand = new List<NetworkObjects.PlayerListing.Card>();
            }
            server.Send(playerListing, clientId);
        }
        private void SendPlayerListingToPlayer(Guid clientId)
        {
            server.Send(this.playerListing, clientId);
        }

        private void DeletePlayer(Guid newClientId)
        {
            if (this.playerListing.Players.Count() != 0)
            {
                string name = "";
                foreach (NetworkObjects.PlayerListing.Player player in this.playerListing.Players)
                {

                    if (player.clientId == newClientId)
                    {
                        name = player.Name;
                    }
                }
                this.playerListing.DeletePlayer(newClientId);

                NetworkObjects.Request request = new NetworkObjects.Request();
                request.Success = true;

                server.Send(this.playerListing, newClientId);

                Console.WriteLine("PLayer " + name + " has left the game.");
            }

        }
        private void SendPlayerJoinFailedMessage(Guid clientId, String reason)
        {
            NetworkObjects.JoinGameResponse response = new NetworkObjects.JoinGameResponse();
            response.Success = false;
            response.ResponseMessage = reason;
            server.Send(response, clientId);
        }

        private void AddPlayerToGame(String name, String username, Guid newClientId)
        {
            this.playerListing.AddPlayer(name, username, newClientId);
            foreach (NetworkObjects.PlayerListing.Player player in this.playerListing.Players)
            {
                if (player.clientId == newClientId)
                    continue;

                NetworkObjects.PlayerJoined newPlayerMsg = new NetworkObjects.PlayerJoined();
                newPlayerMsg.Name = name;
                newPlayerMsg.username = username;
                newPlayerMsg.clientId = newClientId;
                server.Send(newPlayerMsg, player.clientId);
            }
            foreach (NetworkObjects.PlayerListing.Player player in this.playerListing.Players)
            {
                NetworkObjects.PlayerListing.UpdateListPlayers updateListPlayers = new NetworkObjects.PlayerListing.UpdateListPlayers();
                updateListPlayers.Players = playerListing.Players;
                server.Send(updateListPlayers, player.clientId);
            }
            Console.WriteLine("El jugador " + name + " con el usuario " + username + " se ha unido.\n----------------------------------------------------\n");
        }
    }
}