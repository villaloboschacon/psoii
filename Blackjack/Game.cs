﻿using EasyNetwork;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Blackjack
{
    public partial class Game : Form
    {
        Client client = new Client("tcp://127.0.0.1:3000");
        List<Player> lista = new List<Player>();
        Player jugador = new Player("", new Guid());
        public int clicked = 1;
        public bool[] flags;
        Thread newThread = null;
        Thread newThread2 = null;
        public Game(Guid idpartida, string username, string name)
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
            flags = new bool[8];
            //flag 0 = la respuesta de la apuesta
            //flag 1 = si ya paso por la apuesta
            //flag 2 = la respuesta de la cartas(repartir)
            //flag 3 = si ya paso por la repartida.
            //flag 4 = la respuesta del turno
            //flag 5 = si ya paso por el turno
            //flag 6 = turno del dealer
            //flag 7 = fin del juego
            client.DataReceived += Client_DataReceived;
            client.Start();

            NetworkObjects.OpenTable openTable = new NetworkObjects.OpenTable();
            openTable.idpartida = idpartida;
            openTable.name = name;
            openTable.username = username;
            client.Send(openTable);
            jugador.Name = name;
            jugador.username = username;
            gameTable.create_Deck(1);
            gameTable.shuffle_Deck();

        }
        private void Client_DataReceived(object receivedObject)
        {
            if (receivedObject is NetworkObjects.OpenTable)
            {
                NetworkObjects.OpenTable openTable = receivedObject as NetworkObjects.OpenTable;
                if (openTable.response)
                {

                }
                else
                {

                }
            }
            else if (receivedObject is NetworkObjects.Game.Match)
            {
                NetworkObjects.Game.Match match = receivedObject as NetworkObjects.Game.Match;
                lista = new List<Player>();
                foreach (NetworkObjects.PlayerListing.Player player in match.players)
                {
                    Player newPlayer = new Player(player.Name, player.clientId, player.bet, player.username);
                    gameTable.add_Player(newPlayer);
                    lista.Add(newPlayer);
                }
                int i = 0;
                foreach (NetworkObjects.PlayerListing.Player player in match.players)
                {
                    Player newPlayer = new Player(player.Name, player.clientId, player.bet, player.username);
                    if (player.username != gameTable.table[i].username)
                    {
                        gameTable.add_Player(newPlayer);
                    }
                    lista.Add(newPlayer);
                    i++;
                }
                i = 0;
                foreach (NetworkObjects.PlayerListing.Player player in match.players)
                {
                    this.label2.Visible = false;
                    this.label3.Visible = false;
                    this.label4.Visible = false;
                    this.label5.Visible = false;
                    Player nuevo = new Player(player.Name, player.clientId, player.bet, player.username);
                    if (i == 0)
                    {
                        this.label1.Visible = true;
                        label1.Text = player.Name;
                    }
                    else if (i == 1)
                    {
                        this.label2.Visible = true;
                        label2.Text = player.Name;
                    }
                    else if (i == 2)
                    {
                        this.label3.Visible = true;
                        label3.Text = player.Name;
                    }
                    else if (i == 3)
                    {
                        this.label4.Visible = true;
                        label4.Text = player.Name;
                    }
                    else if (i == 4)
                    {
                        this.label5.Visible = true;
                        label5.Text = player.Name;
                    }
                    i++;
                }

            }
            else if (receivedObject is NetworkObjects.PlayerListing.Player)
            {
                NetworkObjects.PlayerListing.Player userRequest = receivedObject as NetworkObjects.PlayerListing.Player;
                jugador.username = userRequest.username;
                jugador.Name = userRequest.Name;
                jugador.ID = userRequest.clientId;

            }
            else if (receivedObject is NetworkObjects.BetResponse)
            {
                NetworkObjects.BetResponse betResponse = receivedObject as NetworkObjects.BetResponse;
                if (betResponse.estado == "listo")
                {
                    flags[0] = true;
                    flags[1] = true;
                }
                else
                {
                    flags[0] = false;
                    flags[1] = true;
                }
            }
            else if (receivedObject is NetworkObjects.DealerResponse)
            {
                NetworkObjects.DealerResponse dealerResponse = receivedObject as NetworkObjects.DealerResponse;
                if (dealerResponse.estado)
                {
                    flags[2] = true;
                    flags[3] = true;
                }
                else
                {
                    flags[2] = false;
                    flags[3] = true;
                }
            }
            else if (receivedObject is NetworkObjects.Turn)
            {
                NetworkObjects.Turn turn = receivedObject as NetworkObjects.Turn;
                if (turn.response == "comienza")
                {
                    flags[4] = true;
                    flags[5] = true;
                }
                else if (turn.response == "dealer")
                {
                    flags[6] = true;
                    turndealer();
                }
                else
                {
                    flags[4] = false;
                    flags[5] = true;
                }
            }
            else if (receivedObject is NetworkObjects.ListPlayersMatch listPlayersMatch)
            {
                NetworkObjects.ListPlayersMatch playerListing = receivedObject as NetworkObjects.ListPlayersMatch;
                actualizar(playerListing);
                int i = 0;
                foreach (NetworkObjects.PlayerListing.Player player in playerListing.players)
                {
                    this.label2.Visible = false;
                    this.label3.Visible = false;
                    this.label4.Visible = false;
                    this.label5.Visible = false;
                    Player nuevo = new Player(player.Name, player.clientId, player.bet, player.username);
                    if (i == 0)
                    {
                        this.label1.Visible = true;
                        label1.Text = player.Name;
                    }
                    else if (i == 1)
                    {
                        this.label2.Visible = true;
                        label2.Text = player.Name;
                    }
                    else if (i == 2)
                    {
                        this.label3.Visible = true;
                        label3.Text = player.Name;
                    }
                    else if (i == 3)
                    {
                        this.label4.Visible = true;
                        label4.Text = player.Name;
                    }
                    else if (i == 4)
                    {
                        this.label5.Visible = true;
                        label5.Text = player.Name;
                    }
                    i++;
                }
                this.Refresh();
            }
        }
        public int posicion()
        {
            for (int i = 0; i < gameTable.Tablesize(); i++)
            {
                if (jugador != null)
                {
                    if (jugador.Name == gameTable.table[i].get_Name())
                    {
                        return i;
                    }
                }

            }
            return -1;
        }
        private void button7_Click(object sender, EventArgs e)
        {

            if (gameTable.table[posicion()].get_Money() >= gameTable.table[posicion()].get_tempBet() + 25)
            {
                this.button1.Enabled = true;
                gameTable.table[posicion()].set_tempBet(25);
                this.textBox1.Text = "Apuesta: $" + gameTable.table[posicion()].get_tempBet().ToString();
            }
            else
            {
                MessageBox.Show(new Form() { TopMost = true }, "La cantidad de dinero que aposto es mayor a su dinero...", "Dinero insuficiente", MessageBoxButtons.OK);
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {

            if (gameTable.table[posicion()].get_Money() >= gameTable.table[posicion()].get_tempBet() + 1)
            {
                this.button1.Enabled = true;
                gameTable.table[posicion()].set_tempBet(1);
                this.textBox1.Text = "Apuesta: $" + gameTable.table[posicion()].get_tempBet().ToString();
            }
            else
            {
                MessageBox.Show(new Form() { TopMost = true }, "La cantidad de dinero que aposto es mayor a su dinero...", "Dinero insuficiente", MessageBoxButtons.OK);
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (gameTable.table[posicion()].get_Money() >= gameTable.table[posicion()].get_tempBet() + 5)
            {
                this.button1.Enabled = true;
                gameTable.table[posicion()].set_tempBet(5);
                this.textBox1.Text = "Apuesta: $" + gameTable.table[posicion()].get_tempBet().ToString();
            }
            else
            {
                MessageBox.Show(new Form() { TopMost = true }, "La cantidad de dinero que aposto es mayor a su dinero...", "Dinero insuficiente", MessageBoxButtons.OK);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (gameTable.table[posicion()].get_Money() >= gameTable.table[posicion()].get_tempBet() + 10)
            {

                this.button1.Enabled = true;
                gameTable.table[posicion()].set_tempBet(10);
                this.textBox1.Text = "Apuesta: $" + gameTable.table[posicion()].get_tempBet().ToString();
            }
            else
            {
                MessageBox.Show(new Form() { TopMost = true }, "La cantidad de dinero que aposto es mayor a su dinero...", "Dinero insuficiente", MessageBoxButtons.OK);
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (gameTable.table[posicion()].get_Money() >= gameTable.table[posicion()].get_tempBet() + 50)
            {

                this.button1.Enabled = true;
                gameTable.table[posicion()].set_tempBet(50);
                this.textBox1.Text = "Apuesta: $" + gameTable.table[posicion()].get_tempBet().ToString();
            }
            else
            {
                MessageBox.Show(new Form() { TopMost = true }, "La cantidad de dinero que aposto es mayor a su dinero...", "Dinero insuficiente", MessageBoxButtons.OK);
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (gameTable.table[posicion()].get_Money() >= gameTable.table[posicion()].get_tempBet() + 100)
            {
                this.button1.Enabled = true;
                gameTable.table[posicion()].set_tempBet(100);
                this.textBox1.Text = "Apuesta: $" + gameTable.table[posicion()].get_tempBet().ToString();
            }
            else
            {
                MessageBox.Show(new Form() { TopMost = true }, "La cantidad de dinero que apostara es mayor a su dinero...", "Dinero insuficiente", MessageBoxButtons.OK);
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            this.button1.Enabled = false;
            gameTable.table[posicion()].set_tempBet(0);
            this.textBox1.Text = "Apuesta: $0";
        }
        private void turns()
        {
            if (!flags[7])
            {
                while (!flags[4])
                {

                    if (flags[4])
                    {
                        this.button1.Enabled = true;
                        this.button2.Enabled = true;
                        this.button3.Enabled = true;
                        this.button4.Enabled = true;
                        this.button5.Enabled = true;
                        this.button6.Enabled = true;
                        this.button7.Enabled = true;
                        this.button8.Enabled = true;
                        this.button9.Enabled = true;
                        this.button10.Enabled = true;

                    }
                    else
                    {
                        this.button1.Enabled = false;
                        this.button2.Enabled = false;
                        this.button3.Enabled = false;
                        this.button4.Enabled = false;
                        this.button5.Enabled = false;
                        this.button6.Enabled = false;
                        this.button7.Enabled = false;
                        this.button8.Enabled = false;
                        this.button9.Enabled = false;
                        this.button10.Enabled = false;
                    }

                }
            }
            else
            {

            }      

        }
        private void check()
        {
            for (int i = 0; i < gameTable.Tablesize(); i++)
            {
                if (gameTable.table[i] != null)
                {
                    if (gameTable.table[i].sum_Hand(true) > 21)
                    {
                        MessageBox.Show(new Form() { TopMost = true }, "La casa gana...", "Has perdido.", MessageBoxButtons.OK);
                        flags[7] = true;
                        gameTable.clearAll();
                    }
                }
            }
        }
        private void turndealer()
        {
            for (int i = 0; i < gameTable.Tablesize(); i++)
            {
                if (gameTable.table[i] != null)
                {
                    if (gameTable.table[i].sum_Hand(true) != 21)
                    {
                        while (gameTable.table[i].sum_Hand(true) > gameTable.dealer.sum_Hand(true))
                        {
                            gameTable.deal_Card_To_Person(5);

                        }
                        for (int j= 2; j < gameTable.dealer.hand.Count(); j++)
                        {
                            switch (j)
                            {
                                case 2: this.pictureBox3.Image = gameTable.dealer.hand[j].image;
                                    this.pictureBox3.Visible = true;
                                    break;
                                case 3: this.pictureBox4.Image = gameTable.dealer.hand[j].image;
                                    this.pictureBox4.Visible = true;
                                    break;
                                case 4: this.pictureBox5.Image = gameTable.dealer.hand[j].image;
                                    this.pictureBox5.Visible = true;
                                    break;
                            }
                            this.label12.Text = gameTable.dealer.sum_Hand(true).ToString();
                        }

                        if (gameTable.dealer.sum_Hand(true)<22 && gameTable.table[i].sum_Hand(true) < gameTable.dealer.sum_Hand(true))
                        {
                            MessageBox.Show(new Form() { TopMost = true }, "La casa gana...", "Has perdido.", MessageBoxButtons.OK);
                            flags[7] = true;
                            gameTable.clearAll();
                        }
                        else
                        {
                            MessageBox.Show(new Form() { TopMost = true }, "Ha ganado el jugador "+ gameTable.table[i].Name, "Has ganado.", MessageBoxButtons.OK);
                        }
                    }
                    else if (gameTable.table[i].sum_Hand(true) == 21)
                    {
                        MessageBox.Show(new Form() { TopMost = true }, "Ha ganado el jugador " + gameTable.table[i].Name, "Has ganado.", MessageBoxButtons.OK);
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            NetworkObjects.UpdateBet updateBet = new NetworkObjects.UpdateBet();


            if (gameTable.table[posicion()].get_tempBet() != 0)
            {
                gameTable.table[posicion()].set_Bet(gameTable.table[posicion()].get_tempBet());
                updateBet.username = gameTable.table[posicion()].username;
                updateBet.name = gameTable.table[posicion()].Name;
                updateBet.bet = gameTable.table[posicion()].bet;
                client.Send(updateBet);
            }
            while (!flags[0])
            {
                using (WaitForm wfrm = new WaitForm(SaveData, "Verificando apuestas..."))
                {
                    wfrm.Refresh();
                    wfrm.ShowDialog(this);
                }
                if (flags[1])
                {
                    if (!flags[0])
                    {
                        MessageBox.Show(new Form() { TopMost = true }, "Esperando apuestas...", "Faltan participantes.", MessageBoxButtons.OK);
                        updateBet.username = gameTable.table[posicion()].username;
                        updateBet.name = gameTable.table[posicion()].Name;
                        updateBet.bet = gameTable.table[posicion()].bet;
                        client.Send(updateBet);
                    }
                }
            }
            NetworkObjects.DealerRequest dealerRequest = new NetworkObjects.DealerRequest();
            client.Send(dealerRequest);
            while (!flags[2])
            {
                using (WaitForm wfrm = new WaitForm(SaveData, "Tomando cartas..."))
                {
                    wfrm.Refresh();
                    wfrm.ShowDialog(this);
                }
                if (flags[3])
                {
                    gameTable.test_Round();
                    NetworkObjects.PlayerListing dealers = new NetworkObjects.PlayerListing();
                    for (int i = 0; i < gameTable.dealer.hand.Count(); i++)
                    {
                        NetworkObjects.PlayerListing.Card card = new NetworkObjects.PlayerListing.Card();
                        card.suit = gameTable.dealer.hand[i].suit;
                        card.number = gameTable.dealer.hand[i].score;
                        card.type = gameTable.dealer.hand[i].rank;
                        dealers.Dealers.hand.Add(card);
                    }
                    for (int i = 0; i < gameTable.Tablesize(); i++)
                    {
                        dealers.AddPlayer(gameTable.table[i].Name, gameTable.table[i].username, gameTable.table[i].ID);
                        for (int j = 0; j < gameTable.table[i].hand.Count(); j++)
                        {
                            NetworkObjects.PlayerListing.Card card = new NetworkObjects.PlayerListing.Card();
                            card.suit = gameTable.table[i].hand[j].suit;
                            card.number = gameTable.table[i].hand[j].score;
                            card.type = gameTable.table[i].hand[j].rank;
                            dealers.Players[i].hand.Add(card);
                        }
                    }
                    client.Send(dealers);
                    flags[2] = true;
                    flags[3] = false;
                }
                else
                {
                    MessageBox.Show(new Form() { TopMost = true }, "Esperando las cartas...", "Repartiendo cartas.", MessageBoxButtons.OK);
                }
            }

            if (gameTable.dealer.hand != null)
            {
                this.pictureBox1.Image = gameTable.dealer.hand[0].image;
                if (!gameTable.dealer.hand[1].visible)
                {
                    this.pictureBox2.Image = Blackjack.Properties.Resources.backcard;
                }
                this.pictureBox2.Image = gameTable.dealer.hand[1].image;
                this.pictureBox1.Visible = true;
                this.pictureBox2.Visible = true;
                this.label12.Text = gameTable.dealer.sum_Hand(true).ToString();
                this.label12.Visible = true;
                this.label11.Text = "Dealer";
                this.label11.Visible = true;
                if (gameTable.table != null)
                {
                    for (int i = 0; i < gameTable.Tablesize(); i++)
                    {
                        this.label13.Visible = true;
                        this.label14.Visible = true;
                        this.label15.Visible = true;
                        this.label13.Text = "$"+gameTable.table[posicion()].bet.ToString();
                        this.label16.Visible = true;
                        this.label16.Text = "$" + gameTable.table[posicion()].money.ToString();
                        switch (i)
                        {
                            case 0:
                                this.pictureBox6.Image = gameTable.table[i].hand[0].image;
                                this.pictureBox7.Image = gameTable.table[i].hand[1].image;
                                this.label6.Text = (gameTable.table[i].sum_Hand(true)).ToString();
                                this.label6.Visible = true;
                                this.pictureBox6.Visible = true;
                                this.pictureBox7.Visible = true;
                                break;
                                /*case 1:
                                    this.pictureBox16.Image = gameTable1.table[i].hand[0].image;
                                    this.pictureBox17.Image = gameTable1.table[i].hand[1].image;
                                    this.person2.Text = (gameTable1.table[i].sum_Hand(true)).ToString();
                                    this.person2.Visible = true;
                                    this.pictureBox16.Visible = true;
                                    this.pictureBox17.Visible = true;
                                    break;
                                case 2:
                                    this.pictureBox18.Image = gameTable1.table[i].hand[0].image;
                                    this.pictureBox19.Image = gameTable1.table[i].hand[1].image;
                                    this.person3.Text = (gameTable1.table[i].sum_Hand(true)).ToString();
                                    this.person3.Visible = true;
                                    this.pictureBox18.Visible = true;
                                    this.pictureBox19.Visible = true;
                                    break;
                                case 3:
                                    this.pictureBox21.Image = gameTable1.table[i].hand[0].image;
                                    this.pictureBox20.Image = gameTable1.table[i].hand[1].image;
                                    this.person4.Text = (gameTable1.table[i].sum_Hand(true)).ToString();
                                    this.person4.Visible = true;
                                    this.pictureBox20.Visible = true;
                                    this.pictureBox21.Visible = true;
                                    break;
                                case 4:
                                    this.pictureBox22.Image = gameTable1.table[i].hand[0].image;
                                    this.pictureBox23.Image = gameTable1.table[i].hand[1].image;
                                    this.person5.Text = (gameTable1.table[i].sum_Hand(true)).ToString();
                                    this.person5.Visible = true;
                                    this.pictureBox22.Visible = true;
                                    this.pictureBox23.Visible = true;
                                    break;*/
                        }
                    }
               
                }
            }
            this.button1.Enabled = false;
            NetworkObjects.Turn turn = new NetworkObjects.Turn();
            turn.isDone = true;
            turn.myturn = true;
            turn.response = jugador.Name;
            turn.name = jugador.Name;
            turn.username = jugador.username;
            client.Send(turn);
            ThreadStart threadDelegate = new ThreadStart(turns);
            Thread newThread = new Thread(threadDelegate);
            newThread.Start();
            while (!gameTable.checkbet())
            {

            }
        }
        private void actualizar(NetworkObjects.ListPlayersMatch list)
        {
            if (gameTable.Tablesize() != 0)
            {
                if (list.players.Count() > gameTable.table.Count())
                {
                    int i = 0;
                    foreach (NetworkObjects.PlayerListing.Player player in list.players)
                    {
                        if (gameTable.table[i] == null)
                        {
                            Player jug = new Player(player.Name, player.clientId);
                            jug.username = player.username;
                            jug.bet = player.bet;

                            if (player.hand.Count() > gameTable.table[i].hand.Count())
                            {
                                for (int j = 0; j < player.hand.Count(); j++)
                                {
                                    Card card = new Card(player.hand[j].type, player.hand[j].suit);
                                    card.score = player.hand[j].number;
                                    jug.hand.Add(card);
                                }
                            }
                            gameTable.add_Player(jug);
                        }
                        else if (player.username == gameTable.table[i].username)
                        {
                            gameTable.table[i].Name = player.Name;
                            gameTable.table[i].username = player.username;
                            gameTable.table[i].bet = player.bet;
                            gameTable.table[i].ID = player.clientId;

                            if (player.hand.Count() > gameTable.table[i].hand.Count())
                            {
                                for (int j = 0; j < player.hand.Count(); j++)
                                {
                                    gameTable.table[i].hand[j].suit = player.hand[j].suit;
                                    gameTable.table[i].hand[j].rank = player.hand[j].type;
                                    gameTable.table[i].hand[j].score = player.hand[j].number;
                                }
                            }
                        }
                        i++;
                    }
                }
                else if (list.players.Count() == gameTable.table.Count())
                {
                    int i = 0;
                    foreach (NetworkObjects.PlayerListing.Player player in list.players)
                    {
                        if (player.username == gameTable.table[i].username)
                        {
                            gameTable.table[i].Name = player.Name;
                            gameTable.table[i].username = player.username;
                            gameTable.table[i].bet = player.bet;
                            gameTable.table[i].ID = player.clientId;

                            if (player.hand.Count() > gameTable.table[i].hand.Count())
                            {
                                for (int j = 0; j < player.hand.Count(); j++)
                                {
                                    gameTable.table[i].hand[j].suit = player.hand[j].suit;
                                    gameTable.table[i].hand[j].rank = player.hand[j].type;
                                    gameTable.table[i].hand[j].score = player.hand[j].number;
                                }
                            }
                        }
                        i++;
                    }
                }
            }
            else
            {
                foreach (NetworkObjects.PlayerListing.Player player in list.players)
                {
                    Player jug = new Player(player.Name, player.clientId);
                    jug.username = player.username;
                    jug.bet = player.bet;
                    gameTable.add_Player(jug);
                }                   
            }
            
        }
        void SaveData()
        {
            for (int i = 0; i <= 300; i++)
            {
                Thread.Sleep(10);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

            if (posicion() != -1)
            {
                gameTable.deal_Card_To_Person(posicion());
            }
            this.clicked++;
            int i = posicion();

            if (gameTable.table[i].sum_Hand(true) == 21)
            {
                this.label1.Text = "BlackJack!";
                this.label1.ForeColor = Color.Black;
                this.label1.Font = new Font("Arial", 30);
            }
            else if (gameTable.table[i].sum_Hand(true) > 21)
            {
                for (int y = 0; y < gameTable.table[i].hand.Count(); y++)
                {

                    if (gameTable.table[i].hand[y].get_Rank() == 'a' && gameTable.table[i].sum_Hand(true) > 21)
                    {
                        gameTable.table[i].hand[y].change_Ace();
                    }
                }
            }

            switch (i)
            {
                case 0:
                    if (this.clicked == 2)
                    {
                        this.pictureBox8.Image = gameTable.table[posicion()].hand[2].image;
                        this.pictureBox8.Visible = true;

                    }
                    if (this.clicked == 3)
                    {
                        this.pictureBox9.Image = gameTable.table[posicion()].hand[3].image;
                        this.pictureBox9.Visible = true;

                    }
                    if (this.clicked == 4)
                    {
                        this.pictureBox10.Image = gameTable.table[posicion()].hand[4].image;
                        this.pictureBox10.Visible = true;

                    }
                    break;
                /*case 1:
                    if (this.clicked == 2)
                    {
                        this.pictureBox24.Image = gameTable1.table[getId()].hand[2].image;
                        this.pictureBox24.Visible = true;
                        this.pictureBox24.BringToFront();
                    }
                    if (this.clicked == 3)
                    {
                        this.pictureBox25.Image = gameTable1.table[getId()].hand[3].image;
                        this.pictureBox25.Visible = true;
                        this.pictureBox25.BringToFront();
                    }
                    if (this.clicked == 4)
                    {
                        this.pictureBox26.Image = gameTable1.table[getId()].hand[4].image;
                        this.pictureBox26.Visible = true;
                        this.pictureBox26.BringToFront();
                    }
                    break;
                case 2:
                    if (this.clicked == 2)
                    {
                        this.pictureBox27.Image = gameTable1.table[getId()].hand[2].image;
                        this.pictureBox27.Visible = true;
                        this.pictureBox27.BringToFront();
                    }
                    if (this.clicked == 3)
                    {
                        this.pictureBox28.Image = gameTable1.table[getId()].hand[3].image;
                        this.pictureBox28.Visible = true;
                        this.pictureBox28.BringToFront();
                    }
                    if (this.clicked == 4)
                    {
                        this.pictureBox29.Image = gameTable1.table[getId()].hand[4].image;
                        this.pictureBox29.Visible = true;
                        this.pictureBox29.BringToFront();
                    }
                    break;
                case 3:
                    if (this.clicked == 2)
                    {
                        this.pictureBox30.Image = gameTable1.table[getId()].hand[2].image;
                        this.pictureBox30.Visible = true;
                        this.pictureBox30.BringToFront();
                    }
                    if (this.clicked == 3)
                    {
                        this.pictureBox31.Image = gameTable1.table[getId()].hand[3].image;
                        this.pictureBox31.Visible = true;
                        this.pictureBox31.BringToFront();
                    }
                    if (this.clicked == 4)
                    {
                        this.pictureBox32.Image = gameTable1.table[getId()].hand[4].image;
                        this.pictureBox32.Visible = true;
                        this.pictureBox32.BringToFront();
                    }
                    break;
                case 4:
                    if (this.clicked == 2)
                    {
                        this.pictureBox33.Image = gameTable1.table[getId()].hand[2].image;
                        this.pictureBox33.Visible = true;
                        this.pictureBox33.BringToFront();
                    }
                    if (this.clicked == 3)
                    {
                        this.pictureBox34.Image = gameTable1.table[getId()].hand[3].image;
                        this.pictureBox34.Visible = true;
                        this.pictureBox34.BringToFront();
                    }
                    if (this.clicked == 4)
                    {
                        this.pictureBox35.Image = gameTable1.table[getId()].hand[4].image;
                        this.pictureBox35.Visible = true;
                        this.pictureBox35.BringToFront();
                    }
                    break;*/
            }
            this.label6.Text = (gameTable.table[posicion()].sum_Hand(true)).ToString();
            NetworkObjects.PlayerListing dealers = new NetworkObjects.PlayerListing();
            for (i = 0; i < gameTable.dealer.hand.Count(); i++)
            {
                NetworkObjects.PlayerListing.Card card = new NetworkObjects.PlayerListing.Card();
                card.suit = gameTable.dealer.hand[i].suit;
                card.number = gameTable.dealer.hand[i].score;
                card.type = gameTable.dealer.hand[i].rank;
                dealers.Dealers.hand.Add(card);
            }
            for (i = 0; i < gameTable.Tablesize(); i++)
            {
                dealers.AddPlayer(gameTable.table[i].Name, gameTable.table[i].username, gameTable.table[i].ID);
                for (int j = 0; j < gameTable.table[i].hand.Count(); j++)
                {
                    NetworkObjects.PlayerListing.Card card = new NetworkObjects.PlayerListing.Card();
                    card.suit = gameTable.table[i].hand[j].suit;
                    card.number = gameTable.table[i].hand[j].score;
                    card.type = gameTable.table[i].hand[j].rank;
                    dealers.Players[i].hand.Add(card);
                }
            }
            client.Send(dealers);
            check();
        }

        private void label14_Click(object sender, EventArgs e)
        {
            NetworkObjects.PlayerListing dealers = new NetworkObjects.PlayerListing();
            for (int i = 0; i < gameTable.dealer.hand.Count(); i++)
            {
                NetworkObjects.PlayerListing.Card card = new NetworkObjects.PlayerListing.Card();
                card.suit = gameTable.dealer.hand[i].suit;
                card.number = gameTable.dealer.hand[i].score;
                card.type = gameTable.dealer.hand[i].rank;
                dealers.Dealers.hand.Add(card);
            }
            for (int i = 0; i < gameTable.Tablesize(); i++)
            {
                dealers.AddPlayer(gameTable.table[i].Name, gameTable.table[i].username, gameTable.table[i].ID);
                for (int j = 0; j < gameTable.table[i].hand.Count(); j++)
                {
                    NetworkObjects.PlayerListing.Card card = new NetworkObjects.PlayerListing.Card();
                    card.suit = gameTable.table[i].hand[j].suit;
                    card.number = gameTable.table[i].hand[j].score;
                    card.type = gameTable.table[i].hand[j].rank;
                    dealers.Players[i].hand.Add(card);
                }
            }
            client.Send(dealers);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.button1.Enabled = false;
            this.button2.Enabled = false;
            this.button3.Enabled = false;
            this.button4.Enabled = false;
            this.button5.Enabled = false;
            this.button6.Enabled = false;
            this.button7.Enabled = false;
            this.button8.Enabled = false;
            this.button9.Enabled = false;
            this.button10.Enabled = false;
            NetworkObjects.Turn turn = new NetworkObjects.Turn();
            turn.isDone = true;
            turn.myturn = false;
            turn.username = jugador.username;
            turn.name = jugador.Name;
            turn.response = "final";
            client.Send(turn);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            this.button11.Visible = false;
            this.button12.Visible = false;
            gameTable.clearAll();
            flags = new bool[8];
            for (int i = 0; i < 8; i++)
            {
                flags[i] = false;
            }
        }
    }
}
