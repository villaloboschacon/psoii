﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Blackjack
{
    public partial class InputName : Form
    {
        private MainForm mainForm;
        public string InputResult { get; set; }
        public string username { get; set; }

        public InputName(MainForm _mainForm)
        {
            this.mainForm = _mainForm;
            InitializeComponent();
           // this.button1.Enabled = true;
           // this.button1.Visible = false;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(textBox1.Text == "" || textBox2.Text == "")
            {
                string message = "Espacios faltantes.";
                string caption = "MARICA";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;
                result = MessageBox.Show(message, caption, buttons);
            }
            else
            {
                InputResult = textBox1.Text;
                username = textBox2.Text;
                DialogResult = DialogResult.OK;
            }

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void InputName_Load(object sender, EventArgs e)
        {

        }
        public void block(bool flag)
        {
            if (flag)
            {
                this.button1.Visible = true;
                this.button1.Enabled = false;
            }
            else
            {
                this.button1.Visible = true;
                this.button1.Enabled = true;
            }
            
        }
    }
}
