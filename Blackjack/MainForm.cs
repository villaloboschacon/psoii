﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;
using EasyNetwork;
using Blackjack.Properties;
using System.Threading;

namespace Blackjack
{
    public partial class MainForm : Form
    {
        Client client = new Client("tcp://127.0.0.1:3000");
        List<Player> lista = new List<Player>();
        InputName tempName;
        public int clicked = 1;
        public bool finish = false;
        public bool user = false;
        public bool boton = false;

        public MainForm()
        {
            tempName = new InputName(this);
            InitializeComponent();
            client.DataReceived += Client_DataReceived;
            client.Start();
        }
        bool var = false;
        bool espera = true;
        bool dealer = false;
        private void Client_DataReceived(object receivedObject)
        {
            if (receivedObject is NetworkObjects.PlayerListing.UpdateListPlayers)
            {
                NetworkObjects.PlayerListing.UpdateListPlayers updateListPlayers = receivedObject as NetworkObjects.PlayerListing.UpdateListPlayers;
                actualizar(receivedObject);
            }
            else if(receivedObject is NetworkObjects.BetResponse)
            {
                NetworkObjects.BetResponse betResponse = receivedObject as NetworkObjects.BetResponse;
                if(betResponse.estado == "listo")
                {
                    espera = false;
                }
                else
                {
                    espera = true;
                }
            }
            else if (receivedObject is NetworkObjects.DealerResponse)
            {
                NetworkObjects.DealerResponse dealerResponse = receivedObject as NetworkObjects.DealerResponse;
                dealer = dealerResponse.estado;
            }
            else if (receivedObject is NetworkObjects.UsernameResponse)
            {
                NetworkObjects.UsernameResponse usernameResponse = receivedObject as NetworkObjects.UsernameResponse;
                var = usernameResponse.Success;
            }
            else if (receivedObject is NetworkObjects.PlayerJoined)
            {
                NetworkObjects.PlayerJoined newPlayerMsg = receivedObject as NetworkObjects.PlayerJoined;
                Player newPlayer = new Player(newPlayerMsg.Name, newPlayerMsg.clientId, newPlayerMsg.bet, newPlayerMsg.username);
            }
            else if (receivedObject is NetworkObjects.JoinGameResponse)
            {
                NetworkObjects.JoinGameResponse joinResponseMsg = receivedObject as NetworkObjects.JoinGameResponse;
                if (joinResponseMsg.Success)
                {
                    //create Blackjack table/cards
                    OnJoinGameSuccess();
                }
            }
            else if (receivedObject is NetworkObjects.PlayerListing)
            {
                actualizar(receivedObject);
                
                NetworkObjects.PlayerListing playerListingMsg = receivedObject as NetworkObjects.PlayerListing;
                if (playerListingMsg.Dealers.hand.Count() == 0)
                {
                    wait = false;
                }
                else
                {
                    wait = true;
                }
                    int i = 0;
                lista = new List<Player>();
                foreach (NetworkObjects.PlayerListing.Player player in playerListingMsg.Players)
                {
                    Player nuevo = new Player(player.Name,player.clientId, player.bet,player.username);
                    lista.Add(nuevo);
                    if (i == 0)
                        SetLabelText(textBox1, player.Name);
                    else if (i == 1)
                        SetLabelText(textBox2, player.Name);
                    else if (i == 2)
                        SetLabelText(textBox3, player.Name);
                    else if (i == 3)
                        SetLabelText(textBox4, player.Name);
                    else if (i == 4)
                        SetLabelText(textBox5, player.Name);
                    i++;
                }
                //this.listBox1.Items.Add(lista);
            }
        }
        bool wait = false;
        public void actualizar(object receivedObject)
        {
            int i = 0;
            if (receivedObject is NetworkObjects.PlayerListing)
            {
                NetworkObjects.PlayerListing playerListingMsg = receivedObject as NetworkObjects.PlayerListing;
                if (playerListingMsg.NumPlayers() != gameTable1.Tablesize())
                {
                    foreach (NetworkObjects.PlayerListing.Player player in playerListingMsg.Players)
                    {
                        Player players = new Player(player.Name, player.clientId, player.bet, player.username);
                        if (gameTable1.table[i] != null)
                        {
                            if (gameTable1.table[i].get_Id() != player.clientId)
                            {
                                int bet = gameTable1.table[i].get_Bet();
                                int money = gameTable1.table[i].get_Money();
                                gameTable1.table[i] = players;
                                gameTable1.table[i].money = money;
                            }
                            else
                            {
                                if (gameTable1.table[i].get_Bet() != player.bet)
                                {
                                    gameTable1.table[i].tempBet = player.bet;
                                    gameTable1.table[i].money = gameTable1.table[i].get_Money() - player.bet;
                                    gameTable1.table[i].bet = player.bet;
                                }
                            }
                        }
                        else
                        {
                            gameTable1.table[i] = players;
                        }
                        if (gameTable1.table[i].hand.Count() != player.hand.Count())
                        {
                            for(int j = 0; j<gameTable1.table[i].hand.Count(); j++)
                            {
                                NetworkObjects.PlayerListing.Card card = new NetworkObjects.PlayerListing.Card();
                                card.number = gameTable1.table[i].hand[j].score;
                                card.suit = gameTable1.table[i].hand[j].suit;
                                card.type = gameTable1.table[i].hand[j].rank;
                                player.hand.Add(card);
                            }
                        }
                        i++;
                    }
                    client.Send(playerListingMsg);
                }
                else
                {
                    foreach (NetworkObjects.PlayerListing.Player player in playerListingMsg.Players)
                    {
                        Player players = new Player(player.Name, player.clientId, player.bet, player.username);
                        if (gameTable1.table[i].get_Id() == player.clientId)
                        {
                            if (gameTable1.table[i].get_Bet() != player.bet)
                            {
                                for (int g = 0; g < player.hand.Count(); g++)
                                {
                                    
                                    Card carta = new Card(player.hand[g].type, playerListingMsg.Players[i].hand[g].suit);
                                    gameTable1.table[i].hand.Add(carta);
                                }                                                              
                                gameTable1.table[i].tempBet = player.bet;
                                gameTable1.table[i].money = gameTable1.table[i].get_Money() - player.bet;
                                gameTable1.table[i].bet = player.bet;
                            }
                            else
                            {
                                if(gameTable1.table[i].hand.Count() == 0)
                                {
                                    for (int g = 0; g < player.hand.Count(); g++)
                                    {
                                        NetworkObjects.PlayerListing.Card card = new NetworkObjects.PlayerListing.Card();
                                        Card carta = new Card(player.hand[g].type, playerListingMsg.Players[i].hand[g].suit);
                                        gameTable1.table[i].hand.Add(carta);
                                    }
                                }

                            }

                            i++;
                        }
                    }
                }
                if(gameTable1.dealer.hand.Count() == 0 && playerListingMsg.Dealers.hand != null)
                {
                    for (int j = 0; j < playerListingMsg.Dealers.hand.Count(); j++)
                    {
                        Card carta = new Card(playerListingMsg.Dealers.hand[j].type, playerListingMsg.Dealers.hand[j].suit);
                        NetworkObjects.PlayerListing.Card card = new NetworkObjects.PlayerListing.Card();
                        gameTable1.dealer.hand.Add(carta);
                    }
                }
            }
            else if (receivedObject is NetworkObjects.PlayerListing.UpdateListPlayers)
            {
                NetworkObjects.PlayerListing.UpdateListPlayers updateListPlayers = receivedObject as NetworkObjects.PlayerListing.UpdateListPlayers;
                int k = 0;
                foreach (NetworkObjects.PlayerListing.Player player in updateListPlayers.Players)
                {
                    if(gameTable1.table[k] != null)
                    {
                        gameTable1.table[k].bet = player.bet;
                        gameTable1.table[k].username = player.username;
                        gameTable1.table[k].Name = player.Name;
                        //gameTable1.table[k].ID = player.clientId;
                        for(int t = 0; t < player.hand.Count();t++)
                        {
                            if(gameTable1.table[k].hand[t] != null)
                            {
                                gameTable1.table[k].hand[t].score = player.hand[t].number;
                                gameTable1.table[k].hand[t].suit = player.hand[t].suit;
                                gameTable1.table[k].hand[t].rank = player.hand[t].type;
                            }
                            else
                            {
                                Card carta = new Card(player.hand[t].type, player.hand[t].suit);
                                carta.score = player.hand[t].number;
                                gameTable1.table[k].hand.Add(carta);
                            }

                        }
                        
                    }
                    else
                    {
                        Player p = new Player(player.Name, player.clientId, player.bet, player.username);
                        for (int t = 0; t < player.hand.Count(); t++)
                        {
                            if (p.hand != null)
                            {
                                p.hand[t].score = player.hand[t].number;
                                p.hand[t].suit = player.hand[t].suit;
                                p.hand[t].rank = player.hand[t].type;
                            }
                            else
                            {
                                Card carta = new Card(player.hand[t].type, player.hand[t].suit);
                                carta.score = player.hand[t].number;
                                p.hand.Add(carta);
                            }
                        }
                        gameTable1.add_Player(p);
                    }

                }
            }
            OnJoinGameSuccess();
        }


        delegate void SetLabelTextCallback(Label label, String text);

        private void SetLabelText(Label label, String text)
        {
            if (label.InvokeRequired)
            {
                SetLabelTextCallback callback = new SetLabelTextCallback(SetLabelText);
                this.Invoke(callback, new object[] { label, text });
            }
            else
            {
                label.Text = text;
                label.Visible = true;
                label.TextAlign = ContentAlignment.MiddleCenter;
            }
        }

        delegate void OnJoinGameSuccessCallback();
        bool create = true;
        private void OnJoinGameSuccess()
        {
            if (button5.InvokeRequired)
            {
                OnJoinGameSuccessCallback callback = new OnJoinGameSuccessCallback(OnJoinGameSuccess);
                this.Invoke(callback, new object[] { });
            }
            else
            {
                if (create)
                {
                    gameTable1.create_Deck(1);
                    gameTable1.shuffle_Deck();
                    create = false;
                }

                //listBox1.Items.Add(room.getPlay());
                this.labelJoiningGame.Visible = false;

                this.menuItem5.Enabled = true;
                this.button5.Visible = true;
                this.button6.Visible = true;
                this.button7.Visible = true;

                this.button3.Visible = false;
                this.button4.Visible = false;

                this.richTextBox1.Visible = true;
                this.richTextBox2.Visible = true;

                this.pictureBox2.Enabled = true;
                this.pictureBox3.Enabled = true;
                this.pictureBox4.Enabled = true;
                this.pictureBox5.Enabled = true;
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.Size = new Size(1580, 960);
            pictureBox2.BackColor = Color.Transparent;
            this.Text = "BlackJack " + varplay;
            // Change parent for overlay PictureBox...
            pictureBox2.Parent = pictureBox1;

        }

        private void MainForm_FormClosing(object sender, EventArgs e)
        {
            request("salir");
            client.Stop();
        }

        private void menuItem5_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void menuItem7_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void menuItem6_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Blackjack Game Rules! \n 1. Dealer deals the cards \n 2. Dealer deals the cards \n 3. Dealer deals the cards \n 4. Dealer deals the cards \n 5. Dealer deals the cards \n 6. Dealer deals the cards \n 7. Dealer deals the cards Dealer deals the cards");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.button3.Visible = true;
            this.button4.Visible = true;
            this.button1.Visible = false;
            this.button2.Visible = false;

            this.button3.BringToFront();
            this.button4.BringToFront();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            //create Blackjack table/cards
            gameTable1.create_Deck(1);
            gameTable1.shuffle_Deck();

            this.menuItem5.Enabled = true;
            this.button5.Visible = true;
            this.button6.Visible = true;
            this.button7.Visible = true;

            this.button3.Visible = false;
            this.button4.Visible = false;

            this.richTextBox1.Visible = true;
            this.richTextBox2.Visible = true;

            if (tempName.ShowDialog() == DialogResult.OK)
            {
                MessageBox.Show("To start select the bet amount by clicking the Chips and then click Bet!");
                Player one = new Player(tempName.InputResult, new Guid());

                MainForm.gameTable1.add_Player(one);

                textBox1.Text = one.get_Name();
                textBox1.Visible = true;

                textBox2.Visible = true;
                textBox3.Visible = true;
                textBox4.Visible = true;
                textBox5.Visible = true;

                textBox1.TextAlign = ContentAlignment.MiddleCenter;
                textBox2.TextAlign = ContentAlignment.MiddleCenter;
                textBox3.TextAlign = ContentAlignment.MiddleCenter;
                textBox4.TextAlign = ContentAlignment.MiddleCenter;
                textBox5.TextAlign = ContentAlignment.MiddleCenter;
                textBox6.TextAlign = ContentAlignment.MiddleCenter;
            }
            this.pictureBox2.Enabled = true;
            this.pictureBox3.Enabled = true;
            this.pictureBox4.Enabled = true;
            this.pictureBox5.Enabled = true;

        }

        // Join active game
        string varplay;
        private void button4_Click(object sender, EventArgs e)
        {
            this.button3.Visible = false;
            this.button4.Visible = false;

            InputName input = new InputName(this);
            if (input.ShowDialog() == DialogResult.OK)
            {
                
                string userna = input.username;
                NetworkObjects.Username username = new NetworkObjects.Username();
                username.user = userna;
                client.Send(username);
                using (WaitForm wfrm = new WaitForm(SaveData, "Uniendose..."))
                {
                    wfrm.ShowDialog(this);
                    while (var)
                    {
                        input.block(true);
                    }
                }
                input.block(false);
                if (!var)
                {
                    string message = "Username ocupado.";
                    string caption = "MARICA";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    DialogResult result;
                    result = MessageBox.Show(message, caption, buttons);
                }
                else
                {
                    Player one = new Player(input.InputResult, new Guid());
                    varplay = input.InputResult;
                    one.username = input.username;

                    this.labelJoiningGame.Visible = true;
                    NetworkObjects.JoinGame joinGameMsg = new NetworkObjects.JoinGame();
                    joinGameMsg.Name = input.InputResult;
                    joinGameMsg.username = input.username;
                    joinGameMsg.bet = 0;

                    //envia jugador.
                    client.Send(joinGameMsg);
                    //MainForm.gameTable1.add_Player(one);
                    request("lista");
                    //solicitud de lista.
                    user = true;
                }

            }

            /*
            this.button5.Visible = true;
            this.button6.Visible = true;
            this.button7.Visible = true;
            this.button8.Visible = true;
            this.button9.Visible = true;

            this.richTextBox1.Visible = true;
            this.richTextBox2.Visible = true;
            */
        }
        public void request(string texto)
        {
            NetworkObjects.Request request = new NetworkObjects.Request();
            request.type = texto;
            client.Send(request);
        }
        public int getId()
        {
            for (int i = 0; i < gameTable1.Tablesize(); i++)
            {
                if (varplay == gameTable1.table[i].get_Name())
                {
                    return i;
                }
            }
            return -1;
        }
        private void button5_Click(object sender, EventArgs e)
        {
            this.textBox6.Visible = true;
            this.button6.Enabled = true;
            this.button7.Enabled = true;

            if (getId() != -1)
            {
                gameTable1.table[getId()].set_Bet(gameTable1.table[getId()].get_tempBet());
                this.richTextBox2.Text = "Pot: $" + gameTable1.table[getId()].get_Money().ToString();
                
                NetworkObjects.UpdateBet updateBet = new NetworkObjects.UpdateBet();
                updateBet.bet = gameTable1.table[getId()].bet;
                client.Send(updateBet);
            }
             while (espera)
             {
                string message = "Esperando apuestas...";
                string caption = "MARICA";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;
                result = MessageBox.Show(message, caption, buttons);
                if(result == DialogResult.OK)
                {
                    request("lista");
                    if (espera)
                    {
                        if (gameTable1.checkbet())
                        {
                            espera = false;
                        }
                    }
                }

             }
            using (WaitForm wfrm = new WaitForm(SaveData, "Chequeando jugadores..."))
            {
                NetworkObjects.DealerRequest dealerRequest = new NetworkObjects.DealerRequest();
                client.Send(dealerRequest);
                wfrm.Refresh();
                wfrm.ShowDialog(this);

            }
            while (!gameTable1.checkbet())
            {
                MessageBox.Show("No todos los usuario han apostado. Espere");
            }

            if (dealer)
            {
                while (dealer)
                {
                    using (WaitForm wfrm = new WaitForm(SaveData, "Chequeando jugadores..."))
                    {
                        wfrm.Refresh();
                        wfrm.ShowDialog(this);
                    }
                    if (dealer)
                    {
                        gameTable1.test_Round();
                        NetworkObjects.PlayerListing dealers = new NetworkObjects.PlayerListing();
                        for (int i = 0; i < gameTable1.dealer.hand.Count(); i++)
                        {
                            NetworkObjects.PlayerListing.Card card = new NetworkObjects.PlayerListing.Card();
                            card.suit = gameTable1.dealer.hand[i].suit;
                            card.number = gameTable1.dealer.hand[i].score;
                            card.type = gameTable1.dealer.hand[i].rank;
                            dealers.Dealers.hand.Add(card);
                        }
                        for (int i = 0; i < gameTable1.Tablesize(); i++)
                        {
                            dealers.AddPlayer(gameTable1.table[i].Name, gameTable1.table[i].username, gameTable1.table[i].ID);
                            for (int j = 0; j < gameTable1.table[i].hand.Count(); j++)
                            {
                                NetworkObjects.PlayerListing.Card card = new NetworkObjects.PlayerListing.Card();
                                card.suit = gameTable1.table[i].hand[j].suit;
                                card.number = gameTable1.table[i].hand[j].score;
                                card.type = gameTable1.table[i].hand[j].rank;
                                dealers.Players[i].hand.Add(card);
                            }
                        }
                        client.Send(dealers);
                        dealer = false;
                    }
                }
            }
            else
            {
                request("lista");
                using (WaitForm wfrm = new WaitForm(SaveData, "Chequeando jugadores..."))
                {
                    wfrm.Refresh();
                    wfrm.ShowDialog(this);
                }
            }
            while (wait)
            {
                request("lista");
                using (WaitForm wfrm = new WaitForm(SaveData, "Chequeando jugadores..."))
                {
                    wfrm.Refresh();
                    wfrm.ShowDialog(this);
                }
                if(gameTable1.dealer.hand.Count() != 0 && gameTable1.table[gameTable1.Tablesize()-1].hand.Count() != 0)
                {
                    wait = false;
                }
            }
            for (int i = 0; i < 5; i++)
            {
                if (gameTable1.table[i] != null)
                {
                    switch (i)
                    {
                        case 0:
                            this.pictureBox6.Image = gameTable1.table[i].hand[0].image;
                            this.pictureBox7.Image = gameTable1.table[i].hand[1].image;
                            this.person1.Text = (gameTable1.table[i].sum_Hand(true)).ToString();
                            this.person1.Visible = true;
                            this.pictureBox6.Visible = true;
                            this.pictureBox7.Visible = true;
                            break;
                        case 1:
                            this.pictureBox16.Image = gameTable1.table[i].hand[0].image;
                            this.pictureBox17.Image = gameTable1.table[i].hand[1].image;
                            this.person2.Text = (gameTable1.table[i].sum_Hand(true)).ToString();
                            this.person2.Visible = true;
                            this.pictureBox16.Visible = true;
                            this.pictureBox17.Visible = true;
                            break;
                        case 2:
                            this.pictureBox18.Image = gameTable1.table[i].hand[0].image;
                            this.pictureBox19.Image = gameTable1.table[i].hand[1].image;
                            this.person3.Text = (gameTable1.table[i].sum_Hand(true)).ToString();
                            this.person3.Visible = true;
                            this.pictureBox18.Visible = true;
                            this.pictureBox19.Visible = true;
                            break;
                        case 3:
                            this.pictureBox21.Image = gameTable1.table[i].hand[0].image;
                            this.pictureBox20.Image = gameTable1.table[i].hand[1].image;
                            this.person4.Text = (gameTable1.table[i].sum_Hand(true)).ToString();
                            this.person4.Visible = true;
                            this.pictureBox20.Visible = true;
                            this.pictureBox21.Visible = true;
                            break;
                        case 4:
                            this.pictureBox22.Image = gameTable1.table[i].hand[0].image;
                            this.pictureBox23.Image = gameTable1.table[i].hand[1].image;
                            this.person5.Text = (gameTable1.table[i].sum_Hand(true)).ToString();
                            this.person5.Visible = true;
                            this.pictureBox22.Visible = true;
                            this.pictureBox23.Visible = true;
                            break;
                    }
                }
            }
         
            this.pictureBox11.Image = gameTable1.dealer.hand[0].image;
            this.pictureBox12.Image = gameTable1.dealer.hand[1].image;
            // dealer has total cards...

            this.dealerLabel.Text = (gameTable1.dealer.sum_Hand(true)).ToString();
            this.dealerLabel.Visible = true;




            this.pictureBox6.BringToFront();
            this.pictureBox16.BringToFront();
            this.pictureBox17.BringToFront();
            this.pictureBox7.BringToFront();

            this.button5.Enabled = false;


            //activates player
            gameTable1.table[getId()].activate();

            this.pictureBox11.Visible = true;
            this.pictureBox12.Visible = true;
            this.pictureBox12.BringToFront();

        }

        private void button6_Click(object sender, EventArgs e)
        {
            int i = 0;
            if(getId() != -1)
            {
                i = getId();
            }
            this.clicked++;

            gameTable1.deal_Card_To_Person(i);

            if (gameTable1.table[i].sum_Hand(true) == 21)
            {
                this.label1.Text = "BlackJack!";
                this.label1.ForeColor = Color.Black;
                this.label1.Font = new Font("Arial", 30);
            }

            else if (gameTable1.table[i].sum_Hand(true) > 21)
            {
                for (int y = 0; y < gameTable1.table[i].hand.Count(); y++)
                {

                    if (gameTable1.table[i].hand[y].get_Rank() == 'a' && gameTable1.table[i].sum_Hand(true) > 21)
                    {
                        gameTable1.table[i].hand[y].change_Ace();
                    }
                }
            }

            switch (i)
            {
                    case 0:
                    if (this.clicked == 2)
                    {
                        this.pictureBox8.Image = gameTable1.table[getId()].hand[2].image;
                        this.pictureBox8.Visible = true;
                        this.pictureBox8.BringToFront();
                    }
                    if (this.clicked == 3)
                    {
                        this.pictureBox9.Image = gameTable1.table[getId()].hand[3].image;
                        this.pictureBox9.Visible = true;
                        this.pictureBox9.BringToFront();
                    }
                    if (this.clicked == 4)
                    {
                        this.pictureBox10.Image = gameTable1.table[getId()].hand[4].image;
                        this.pictureBox10.Visible = true;
                        this.pictureBox10.BringToFront();
                    }
                    break;
                case 1:
                    if (this.clicked == 2)
                    {
                        this.pictureBox24.Image = gameTable1.table[getId()].hand[2].image;
                        this.pictureBox24.Visible = true;
                        this.pictureBox24.BringToFront();
                    }
                    if (this.clicked == 3)
                    {
                        this.pictureBox25.Image = gameTable1.table[getId()].hand[3].image;
                        this.pictureBox25.Visible = true;
                        this.pictureBox25.BringToFront();
                    }
                    if (this.clicked == 4)
                    {
                        this.pictureBox26.Image = gameTable1.table[getId()].hand[4].image;
                        this.pictureBox26.Visible = true;
                        this.pictureBox26.BringToFront();
                    }
                    break;
                case 2:
                    if (this.clicked == 2)
                    {
                        this.pictureBox27.Image = gameTable1.table[getId()].hand[2].image;
                        this.pictureBox27.Visible = true;
                        this.pictureBox27.BringToFront();
                    }
                    if (this.clicked == 3)
                    {
                        this.pictureBox28.Image = gameTable1.table[getId()].hand[3].image;
                        this.pictureBox28.Visible = true;
                        this.pictureBox28.BringToFront();
                    }
                    if (this.clicked == 4)
                    {
                        this.pictureBox29.Image = gameTable1.table[getId()].hand[4].image;
                        this.pictureBox29.Visible = true;
                        this.pictureBox29.BringToFront();
                    }
                    break;
                case 3:
                    if (this.clicked == 2)
                    {
                        this.pictureBox30.Image = gameTable1.table[getId()].hand[2].image;
                        this.pictureBox30.Visible = true;
                        this.pictureBox30.BringToFront();
                    }
                    if (this.clicked == 3)
                    {
                        this.pictureBox31.Image = gameTable1.table[getId()].hand[3].image;
                        this.pictureBox31.Visible = true;
                        this.pictureBox31.BringToFront();
                    }
                    if (this.clicked == 4)
                    {
                        this.pictureBox32.Image = gameTable1.table[getId()].hand[4].image;
                        this.pictureBox32.Visible = true;
                        this.pictureBox32.BringToFront();
                    }
                    break;
                case 4:
                    if (this.clicked == 2)
                    {
                        this.pictureBox33.Image = gameTable1.table[getId()].hand[2].image;
                        this.pictureBox33.Visible = true;
                        this.pictureBox33.BringToFront();
                    }
                    if (this.clicked == 3)
                    {
                        this.pictureBox34.Image = gameTable1.table[getId()].hand[3].image;
                        this.pictureBox34.Visible = true;
                        this.pictureBox34.BringToFront();
                    }
                    if (this.clicked == 4)
                    {
                        this.pictureBox35.Image = gameTable1.table[getId()].hand[4].image;
                        this.pictureBox35.Visible = true;
                        this.pictureBox35.BringToFront();
                    }
                    break;
            }
            request("lista");
            // MessageBox.Show("total is " + gameTable1.table[i].sum_Hand());

            this.person1.Text = (gameTable1.table[getId()].sum_Hand(true)).ToString();


            if (gameTable1.table[i].sum_Hand(true) > 21)
            {
                gameTable1.table[i].deactivate();
                // MessageBox.Show("Sorry " + gameTable1.table[i].get_Name() + ", you have bust");
                this.label1.Visible = true;
                this.label1.BringToFront();
                this.label1.BackColor = Color.White;
                this.finish = true;

                this.button5.Enabled = true;
                this.button6.Enabled = false;
                this.button7.Enabled = false;

                this.button12.Visible = true;
                this.button13.Visible = true;

                this.button12.BringToFront();
                this.button13.BringToFront();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            //disable surrender
            request("lista");
            this.button6.Enabled = false;
            gameTable1.dealer.start_Turn();
            gameTable1.dealer.auto_Deal(gameTable1);
            gameTable1.check_Players_Vs_Dealer();
            this.richTextBox2.Text = "Pot: $" + gameTable1.table[getId()].get_Money().ToString();


            for (int i = 1; i < gameTable1.dealer.hand.Count(); i++)
            {
                if (i == 2)
                {
                    this.pictureBox13.Image = gameTable1.dealer.hand[i].image;
                    this.pictureBox13.Visible = true;
                    this.pictureBox13.BringToFront();

                    this.pictureBox11.Location = new Point(337, 154);
                    this.pictureBox12.Location = new Point(365, 154);
                    this.pictureBox13.Location = new Point(393, 154);
                }
                if (i == 3)
                {
                    this.pictureBox14.Image = gameTable1.dealer.hand[i].image;
                    this.pictureBox14.Visible = true;
                    this.pictureBox14.BringToFront();

                    this.pictureBox11.Location = new Point(323, 154);
                    this.pictureBox12.Location = new Point(351, 154);
                    this.pictureBox13.Location = new Point(379, 154);
                    this.pictureBox14.Location = new Point(407, 154);
                }
                if (i == 4)
                {
                    this.pictureBox15.Image = gameTable1.dealer.hand[i].image;
                    this.pictureBox15.Visible = true;
                    this.pictureBox15.BringToFront();

                    this.pictureBox11.Location = new Point(306, 154);
                    this.pictureBox12.Location = new Point(344, 154);
                    this.pictureBox13.Location = new Point(372, 154);
                    this.pictureBox14.Location = new Point(400, 154);
                    this.pictureBox15.Location = new Point(428, 154);
                }
            }
            this.dealerLabel.Text = (gameTable1.dealer.sum_Hand(true)).ToString();
            this.label1.BringToFront();
        }


        private void button9_Click(object sender, EventArgs e)
        {
            gameTable1.print_Table();
        }

        private void menuItem3_Click(object sender, EventArgs e)
        {
            playAudio();
        }

        private void menuItem4_Click(object sender, EventArgs e)
        {
            stopAudio();
        }

        private void playAudio() // defining the function
        {
            SoundPlayer audio = new SoundPlayer(global::Blackjack.Properties.Resources.backgroundMusic); // here WindowsFormsApplication1 is the namespace and Connect is the audio file name
            audio.PlayLooping();
        }

        private void stopAudio() // defining the function
        {
            SoundPlayer audio = new SoundPlayer(global::Blackjack.Properties.Resources.backgroundMusic);
            audio.Stop();
        }

        private void menuItem5_Click_1(object sender, EventArgs e)
        {
            this.button5.Visible = true;
            this.button6.Visible = true;
            this.button7.Visible = true;

            this.button5.Enabled = false;
            this.button6.Enabled = false;
            this.button7.Enabled = false;

            this.button3.Visible = false;
            this.button4.Visible = false;

            this.richTextBox1.Visible = true;
            this.richTextBox2.Visible = true;

            this.button10.Visible = false;

            MessageBox.Show("To start select the bet amount by clicking the Chips and then click Bet!");

            this.pictureBox2.Enabled = true;
            this.pictureBox3.Enabled = true;
            this.pictureBox4.Enabled = true;
            this.pictureBox5.Enabled = true;

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("100 clickd");
            this.button5.Enabled = true;
            this.button10.Visible = true;
            gameTable1.table[getId()].set_tempBet(100);
            this.richTextBox1.Text = "Bet: $" + gameTable1.table[getId()].get_tempBet().ToString();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("25 clickd");
            this.button5.Enabled = true;
            this.button10.Visible = true;
            gameTable1.table[getId()].set_tempBet(25);
            this.richTextBox1.Text = "Bet: $" + gameTable1.table[getId()].get_tempBet().ToString();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("5 clickd");
            this.button5.Enabled = true;
            this.button10.Visible = true;
            gameTable1.table[getId()].set_tempBet(5);
            this.richTextBox1.Text = "Bet: $" + gameTable1.table[getId()].get_tempBet().ToString();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("1 clickd");
            this.button5.Enabled = true;
            this.button10.Visible = true;
            gameTable1.table[getId()].set_tempBet(1);
            this.richTextBox1.Text = "Bet: $" + gameTable1.table[getId()].get_tempBet().ToString();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            gameTable1.table[getId()].reset_tempBet();
            this.richTextBox1.Text = "Bet: $" + gameTable1.table[getId()].get_tempBet().ToString();
        }

        private void button11_Click(object sender, EventArgs e)
        {

        }

        private void button12_Click(object sender, EventArgs e)
        {
            this.finish = true;

            this.clicked = 1;

            gameTable1.table[getId()].sum_Hand(false);

            gameTable1.dealer.sum_Hand(false);

            this.person1.Text = "0";
            this.dealerLabel.Text = "0";

            this.label1.Visible = false;

            this.pictureBox6.Visible = false;
            this.pictureBox7.Visible = false;
            this.pictureBox8.Visible = false;
            this.pictureBox9.Visible = false;
            this.pictureBox10.Visible = false;
            this.pictureBox11.Visible = false;
            this.pictureBox12.Visible = false;
            this.pictureBox13.Visible = false;
            this.pictureBox14.Visible = false;
            this.pictureBox15.Visible = false;

            //this.pictureBox6.Image.Dispose();
            //this.pictureBox7.Image.Dispose();

            this.pictureBox6.Image = null;
            this.pictureBox7.Image = null;
            this.pictureBox8.Image = null;
            this.pictureBox9.Image = null;
            this.pictureBox10.Image = null;
            this.pictureBox11.Image = null;
            this.pictureBox12.Image = null;
            this.pictureBox13.Image = null;
            this.pictureBox14.Image = null;
            this.pictureBox15.Image = null;

            this.button12.Visible = false;
            this.button13.Visible = false;

            this.button5.Enabled = true;
            request("nuevo");
        }

        private void button14_Click(object sender, EventArgs e)
        {
            this.Refresh();
            request("lista");
            listBox1.Items.Clear();
            for (int i = 0; i < lista.Count(); i++)
            {
                //string test = "";
                if (lista[i].get_Bet() != 0)
                {
                    //  test = "\t activo";
                    listBox1.Items.Add((lista[i].Name) + (lista[i].get_Bet()));
                }
                else
                {
                    //test = "\t inactivo";
                    listBox1.Items.Add((lista[i].Name) + ("Aun no apuesta."));
                }
                listBox1.Items.Add((lista[i].Name) + (lista[i].get_Bet()));
            }

        }

        private void button15_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void labelJoiningGame_Click(object sender, EventArgs e)
        {

        }
        void SaveData()
        {
            for(int i = 0; i<= 500; i++)
            {
                Thread.Sleep(10);

            }
        }
        private void button16_Click(object sender, EventArgs e)
        {
            using(WaitForm wfrm = new WaitForm(SaveData,"Default"))
            {
                wfrm.ShowDialog(this);
            }
        }


    }
}