﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Blackjack
{
    public partial class WaitForm : Form
    {
        public String label { get; set; }
        
        public Action Worker { get; set; }
        public WaitForm(Action worker,String title)
        {
            this.Text = title;
            InitializeComponent();
            if (worker == null)
                throw new ArgumentNullException();
            Worker = worker;
        }
       
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Task.Factory.StartNew(Worker).ContinueWith(t => { this.Close(); }, TaskScheduler.FromCurrentSynchronizationContext());
        }
        private void progressBar1_Click(object sender, EventArgs e)
        {

        }
        private void WaitForm_Load(object sender, EventArgs e)
        {

        }
    }
}
