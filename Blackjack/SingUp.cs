﻿using EasyNetwork;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Blackjack
{
    public partial class SingUp : Form
    {
        Client client = new Client("tcp://127.0.0.1:3000");
        private bool [] flags;
        public int xClick = 0, yClick = 0;
        public SingUp()
        {
            InitializeComponent();
            flags = new bool[4];
            flags[1] = false;
            flags[3] = false;
            //flag 0 = respuesta positiva o negativa de la disponibilidad del usuario
            //flag 1 = recibio la respuesta de la disponibilidad del usuario
            //flag 2 = respuesta positiva o negativa de la creacion del juagador
            //flag 3 = recibio la respuesta de crear el jugador
            client.DataReceived += Client_DataReceived;
            client.Start();
        }
        private void Client_DataReceived(object receivedObject)
        {
            if (receivedObject is NetworkObjects.UsernameResponse)
            {
                NetworkObjects.UsernameResponse usernameResponse = receivedObject as NetworkObjects.UsernameResponse;
                if (usernameResponse.Success)
                {
                    flags[0] = true;
                    flags[1] = true;
                }
                else
                {
                    flags[0] = false;
                    flags[1] = true;
                }
                
            }
            else if(receivedObject is  NetworkObjects.JoinGameResponse mJoinResponse)
            {
                NetworkObjects.JoinGameResponse joinGameResponse = receivedObject as NetworkObjects.JoinGameResponse;
                if (joinGameResponse.Success)
                {
                    flags[2] = true;
                    flags[3] = true;
                }
                else
                {
                    flags[2] = false;
                    flags[3] = true;
                }
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void textBox1_Enter(object sender, EventArgs e)
        {
            if (textBox1.Text == "NOMBRE")
            {
                textBox1.Text = "";
            }
            else
            {
                if (flags[1])
                {
                    if (flags[0])
                    {
                        this.pictureBox1.Image = Blackjack.Properties.Resources.check;
                        this.button2.Enabled = true;
                    }
                    else
                    {
                        this.pictureBox1.Image = Blackjack.Properties.Resources.uncheck;
                        this.button2.Enabled = false;
                    }
                }
            }
        }
        private void textBox1_Leave(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox1.Text == " ")
            {
                textBox1.Text = "NOMBRE";
            }
        }
        private void textBox2_Enter(object sender, EventArgs e)
        {
            if (textBox2.Text == "USUARIO")
            {
                textBox2.Text = "";
            }
        }
        private void textBox2_Leave(object sender, EventArgs e)
        {
            
            if (textBox2.Text == "" || textBox2.Text == " ")
            {
                textBox2.Text = "USUARIO";
                this.pictureBox1.Visible = false;
            }
            else
            {
                this.pictureBox1.Visible = true;
                this.button2.Enabled = false;
                NetworkObjects.Username username = new NetworkObjects.Username();
                username.user = textBox2.Text;
                client.Send(username);
                while (!flags[1])
                {
                    this.pictureBox1.Visible = true;
                    this.pictureBox1.Image = Blackjack.Properties.Resources.LOAD;
                }
                if (flags[1])
                {
                    if (flags[0])
                    {
                        this.pictureBox1.Image = Blackjack.Properties.Resources.check;
                        this.button2.Enabled = true;
                        flags[1] = false;
                        flags[0] = false;
                    }
                    else
                    {
                        this.pictureBox1.Image = Blackjack.Properties.Resources.uncheck;
                        this.button2.Enabled = false;
                        flags[1] = false;
                        flags[0] = false;
                    }
                }
            }
            
        }
        void SaveData()
        {
            for (int i = 0; i <= 500; i++)
            {
                Thread.Sleep(10);
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if ((textBox2.Text != "USUARIO" || textBox2.Text == "" || textBox2.Text == " ")&& (textBox1.Text != "NOMBRE" || textBox1.Text == "" || textBox1.Text == " "))
            {
                NetworkObjects.JoinGame joinGame = new NetworkObjects.JoinGame();
                joinGame.Name = textBox1.Text;
                joinGame.username = textBox2.Text;
                client.Send(joinGame);
                while (!flags[3])
                {
                    using (WaitForm wfrm = new WaitForm(SaveData, "Creando usuario..."))
                    {
                        wfrm.ShowDialog(this);
                    }
                }
                if (flags[3])
                {
                    if (flags[2])
                    {
                        this.pictureBox1.Image = Blackjack.Properties.Resources.check;
                        this.button2.Enabled = true;
                        flags[2] = false;
                        flags[3] = false;
                        MessageBox.Show("Usuario creado...");
                        this.Close();
                    }
                    else
                    {
                        this.pictureBox1.Image = Blackjack.Properties.Resources.uncheck;
                        this.button2.Enabled = false;
                        flags[2] = false;
                        flags[3] = false;
                    }
                }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
        private void pictureBox1_BackGroundChange(object sender, EventArgs e)
        {
            if (flags[0])
            {
                this.pictureBox1.Image = Blackjack.Properties.Resources.check;
                this.button2.Enabled = true;
            }
            else
            {
                this.pictureBox1.Image = Blackjack.Properties.Resources.uncheck;
                this.button2.Enabled = false;
            }
        }

        private void SingUp_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
            { xClick = e.X; yClick = e.Y; }
            else
            { this.Left = this.Left + (e.X - xClick); this.Top = this.Top + (e.Y - yClick); }
        }
    }
}
