﻿using EasyNetwork;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Blackjack
{
    public partial class Enter : Form
    {
        Client client = new Client("tcp://127.0.0.1:3000");
        public bool[] flags;
        Player phost;
        Guid idpartida;
        List<NetworkObjects.PlayerListing.Player> jugadores;
        public int xClick = 0, yClick = 0;
        private System.Windows.Forms.Timer timer1;
        private int counter = 4;
        public Enter()
        {
            InitializeComponent();
            client.DataReceived += Client_DataReceived;
            client.Start();
            jugadores = new List<NetworkObjects.PlayerListing.Player>();
            flags = new bool[5];
            //flag 1 = si el juego se creo
            //flag 2 = si se unio a un juego creado
            //flag 3 = iniciar contador
            //flag 4 = si ya envio si puede o no iniciar el contador
            phost = new Player("", new Guid());
            idpartida = new Guid();
        }
        private void Client_DataReceived(object receivedObject)
        {
            if (receivedObject is NetworkObjects.Game.Match)
            {
                NetworkObjects.Game.Match match = receivedObject as NetworkObjects.Game.Match;
                jugadores = new List<NetworkObjects.PlayerListing.Player>();
                foreach (NetworkObjects.PlayerListing.Player player in match.players)
                {
                    jugadores.Add(player);
                }
                actualizar();
            }
            else if (receivedObject is NetworkObjects.Game.GameResponse)
            {
                NetworkObjects.Game.GameResponse gameResponse = receivedObject as NetworkObjects.Game.GameResponse;
                if (gameResponse.tipo == "CREAR")
                {
                    if (gameResponse.estado)
                    {
                        flags[1] = true;
                        idpartida = gameResponse.idpartida;
                    }
                    else
                    {
                        flags[1] = false;
                    }
                }
                else if (gameResponse.tipo == "UNIRSE")
                {
                    if (gameResponse.estado)
                    {
                        flags[2] = true;
                        idpartida = gameResponse.idpartida;
                    }
                    else
                    {
                        flags[2] = false;
                    }
                }
                else if (gameResponse.tipo == "COUNTDOWN")
                {
                    if (gameResponse.estado)
                    {
                        flags[3] = true;
                        flags[4] = true;
                        //idpartida = gameResponse.idpartida;
                    }
                    else
                    {
                        flags[3] = false;
                        flags[4] = true;
                    }
                }

            }
            else if (receivedObject is NetworkObjects.PlayerListing.Player)
            {
                NetworkObjects.PlayerListing.Player player = receivedObject as NetworkObjects.PlayerListing.Player;
                phost.ID = player.clientId;
                phost.Name = player.Name;
                phost.username = player.username;
                phost.active = true;
                NetworkObjects.Online online = new NetworkObjects.Online();
                online.online = true;
                online.inmatch = false;
                online.name = phost.Name;
                online.username = phost.username;
                client.Send(online);

                //this.label4.Text = phost.Name;
                //this.label6.Text = phost.username;
                if (phost.active)
                {
                    //this.pictureBox1.Visible = true;
                }
            }
            else if (receivedObject is NetworkObjects.LoginResponse)
            {
                NetworkObjects.LoginResponse loginResponse = receivedObject as NetworkObjects.LoginResponse;

                if (loginResponse.Success)
                {
                    flags[0] = true;
                }
                else
                {
                    MessageBox.Show(new Form() { TopMost = true }, "Verifique el usuario o el nombre...", "Nombre o usuario incorrecto.", MessageBoxButtons.OK);
                }
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            NetworkObjects.Login login = new NetworkObjects.Login();
            login.username = textBox4.Text;
            login.name = textBox3.Text;
            client.Send(login);
            using (WaitForm wfrm = new WaitForm(SaveData, "Chequeando jugador..."))
            {
                wfrm.Refresh();
                wfrm.ShowDialog(this);
            }
            if (flags[0])
            {
                //para pedir informacion del usuario
                NetworkObjects.UserRequest userRequest = new NetworkObjects.UserRequest();
                userRequest.player.Name = login.name;
                userRequest.player.username = login.username;
                client.Send(userRequest);
                this.pictureBox1.Visible = true;
                this.label1.Visible = false;
                this.label3.Visible = false;
                this.label5.Visible = false;
                this.textBox3.Visible = false;
                this.textBox4.Visible = false;
                this.button1.Visible = false;
                this.button2.Visible = false;
                this.label4.Visible = true;
                this.label6.Visible = true;
                this.listBox1.Visible = false;
                this.radioButton1.Visible = true;
                this.radioButton2.Visible = true;
                this.textBox1.Visible = true;
                this.textBox1.Enabled = false;
                this.textBox2.Visible = true;
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Close();
            this.Dispose();
            // this.ShowDialog();


        }
        private void textBox3_Enter(object sender, EventArgs e)
        {
            if (textBox3.Text == "NOMBRE")
            {
                textBox3.Text = "";
            }
        }
        private void textBox3_Leave(object sender, EventArgs e)
        {
            if (textBox3.Text == "" || textBox3.Text == " ")
            {
                textBox3.Text = "NOMBRE";
            }
        }
        private void textBox4_Enter(object sender, EventArgs e)
        {
            if (textBox4.Text == "USUARIO")
            {
                textBox4.Text = "";
            }
        }
        private void textBox4_Leave(object sender, EventArgs e)
        {
            if (textBox4.Text == "" || textBox4.Text == " ")
            {
                textBox4.Text = "USUARIO";
            }
        }
        private void textBox2_Enter(object sender, EventArgs e)
        {
            if (textBox2.Text == "PASSWORD")
            {
                textBox2.Text = "";
            }
        }
        private void textBox2_Leave(object sender, EventArgs e)
        {
            if (textBox2.Text == "" || textBox2.Text == " ")
            {
                textBox2.Text = "PASSWORD";
            }
        }
        private void actualizar()
        {
            CheckForIllegalCrossThreadCalls = false;
            listBox1.Items.Clear();
            for (int i = 0; i < jugadores.Count(); i++)
            {
                if (jugadores[i].inmatch)
                {
                    listBox1.Items.Add((jugadores[i].Name) + "(" + (jugadores[i].username) + ")");
                }
            }
            this.Refresh();
        }
        private void textBox3_Click2(object sender, EventArgs e)
        {


        }

        private void TextBox3_Click(object sender, KeyPressEventArgs e)
        {

        }
        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
        private void Form1_Load(object sender, System.EventArgs e)
        {
            label1.Font = new Font("Ubuntu", 12, FontStyle.Regular);
        }

        private void label3_Click(object sender, EventArgs e)
        {
            SingUp sing = new SingUp();
            sing.Show();
            //Show(new SingUp());
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Settings settings = new Settings();
            settings.Show();
        }
        void SaveData()
        {
            for (int i = 0; i <= 300; i++)
            {
                Thread.Sleep(10);
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                this.label7.Visible = true;
                this.button2.Visible = true;
                this.button2.Text = "CREAR";
                this.label7.Text = "Inserte una palabra para que se puedan\nunir los demas usuarios a la partida.";
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                this.label7.Visible = true;
                this.button2.Visible = true;
                this.button2.Text = "UNIRSE";
                this.label7.Text = "Inserte la palabra para poder unirse\na la partida ya creada.";
            }
        }

        private void Enter_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
            { xClick = e.X; yClick = e.Y; }
            else
            { this.Left = this.Left + (e.X - xClick); this.Top = this.Top + (e.Y - yClick); }


            this.label4.Text = phost.Name;
            this.label6.Text = phost.username;
            if (phost.active)
            {
                this.pictureBox1.Visible = true;
            }
        }

        private void Enter_FormClosing(object sender, FormClosingEventArgs e)
        {
            //this.Hide();
            //this.Close();
            this.Dispose();
            Application.Exit();
        }

        private void Enter_FormClosed(object sender, FormClosedEventArgs e)
        {
            //this.Hide();
            this.Close();
            this.Dispose();
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (button2.Text == "CREAR")
            {
                NetworkObjects.Game.CreateMatch createMatch = new NetworkObjects.Game.CreateMatch();
                createMatch.pass = textBox2.Text;
                createMatch.nombre = phost.Name;
                createMatch.username = phost.username;
                client.Send(createMatch);
                using (WaitForm wfrm = new WaitForm(SaveData, "Chequeando jugador..."))
                {
                    wfrm.Refresh();
                    wfrm.ShowDialog(this);
                }
                if (flags[1])
                {
                    //MessageBox.Show("Partida creada...");
                    MessageBox.Show(new Form() { TopMost = true }, "Se logro crear la partida con exito...", "Partida creada", MessageBoxButtons.OK);
                    textBox2.Enabled = false;
                    this.listBox1.Visible = true;
                    radioButton1.Enabled = false;
                    radioButton2.Enabled = false;
                    button2.Text = "LISTO";
                    if (idpartida != null)
                    {
                        textBox1.Text = idpartida.ToString();
                    }
                }
                else
                {
                    MessageBox.Show(new Form() { TopMost = true }, "Se ha producido un problema al crear la partida...", "Error al crear la partida", MessageBoxButtons.OK);
                    //MessageBox.Show("");
                }
            }
            else if (button2.Text == "UNIRSE")
            {
                NetworkObjects.Game.JoinMatch joinMatch = new NetworkObjects.Game.JoinMatch();
                joinMatch.password = textBox2.Text;
                joinMatch.username = phost.username;
                client.Send(joinMatch);
                using (WaitForm wfrm = new WaitForm(SaveData, "Chequeando jugador..."))
                {
                    wfrm.Refresh();
                    wfrm.ShowDialog(this);
                }
                if (flags[2])
                {
                    //MessageBox.Show("Partida creada...");
                    MessageBox.Show(new Form() { TopMost = true }, "Se logro unir a la partida con exito...", "Partida creada", MessageBoxButtons.OK);
                    textBox2.Enabled = false;
                    this.listBox1.Visible = true;
                    radioButton1.Enabled = false;
                    radioButton2.Enabled = false;
                    button2.Text = "LISTO";
                    if (idpartida != null)
                    {
                        textBox1.Text = idpartida.ToString();
                    }
                }
                else
                {
                    MessageBox.Show(new Form() { TopMost = true }, "Se ha producido un problema al unirse a la partida...", "Error al unirse a la partida", MessageBoxButtons.OK);
                    //MessageBox.Show("");
                }
            }
            else if (button2.Text == "LISTO")
            {
                button2.Text = "CONFIRMAR";                
                NetworkObjects.Online cerote = new NetworkObjects.Online();
                cerote.username = phost.username;
                cerote.name = phost.Name;
                cerote.online = true;
                cerote.countdown = true;
                cerote.inmatch = true;
                client.Send(cerote);
                using (WaitForm wfrm = new WaitForm(SaveData, "Chequeando jugador..."))
                {
                    wfrm.Refresh();
                    wfrm.ShowDialog(this);
                }
            }
            else if (button2.Text == "VAMOS!")
            {
                this.Hide();
                Game game = new Game(idpartida, phost.username,phost.Name);
                game.Show();
                /*MainForm form = new MainForm();
                form.Show();*/
            }
            else
            {
                timer1 = new System.Windows.Forms.Timer();
                timer1.Tick += new EventHandler(timer1_Tick);
                timer1.Interval = 1000; // 1 second
                timer1.Start();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            counter--;
            if (counter == 0)
                timer1.Stop();
            lblCountDown.Text = counter.ToString();


            if (counter != 0)
            {
                this.button2.Enabled = false;
                this.button2.Text = counter.ToString();
            }
            else
            {
                this.button2.Enabled = true;
                this.button2.Text = "VAMOS!";
                this.button2.PerformClick();
            }
        }
    }
}
